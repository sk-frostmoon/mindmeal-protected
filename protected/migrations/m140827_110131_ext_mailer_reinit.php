<?php

class m140827_110131_ext_mailer_reinit extends CDbMigration
{
	public function up()
	{
        if (file_exists(Yii::getPathOfAlias('application.extensions.mailer'))) {
            exec("( cd " . Yii::getPathOfAlias('application.extensions') . " && rm -rf mailer)");
        }
        exec("(cd " . Yii::getPathOfAlias('application.extensions') . " && git clone gitrepo@dev.mindmeal.ru:repo/yii_extension_mailer mailer)");
	}

	public function down()
	{
		echo "m140827_110131_ext_mailer_reinit does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
