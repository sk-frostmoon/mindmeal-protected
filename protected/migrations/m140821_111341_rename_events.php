<?php

class m140821_111341_rename_events extends CDbMigration
{
	public function up()
	{
        if (file_exists(Yii::getPathOfAlias('application.config') . '/local.php')) {
            exec('sed -i "s/statEvents/events/" "' . Yii::getPathOfAlias('application.config') . '/local.php"');
            exec('sed -i "s/StatEvents/Events/" "' . Yii::getPathOfAlias('application.config') . '/local.php"');
        }
        exec('sed -i "s/statEvents/events/" "' . Yii::getPathOfAlias('application.config') . '/main.php"');
        exec('sed -i "s/StatEvents/Events/" "' . Yii::getPathOfAlias('application.config') . '/main.php"');

        if (file_exists(Yii::getPathOfAlias('application.extensions') . '/statEvents')) {
            exec('mv "' . Yii::getPathOfAlias('application.extensions') . '/statEvents" "' . Yii::getPathOfAlias('application.extensions') . '/events"');
        }

        if (!file_exists(Yii::getPathOfAlias('application.extensions') . '/events')) {
            exec('git clone gitrepo@dev.mindmeal.ru:repo/yii_extensions_statEvents events');
        } else {
            exec('cd "' . Yii::getPathOfAlias('application.extensions') . '/events"');
            exec('git pull origin master');
        }

        echo <<<END

WARN: Extension statEvents renamed as Events.

Patch your config:

  'preload' => array(... 'events', 'mailer'),
  ...
  'components' => array(
        'events' => array(
            'class' => 'ext.events.Events',
        ),
  ),
  ...

END;
	}

	public function down()
	{
		echo "m140821_111341_rename_events does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
