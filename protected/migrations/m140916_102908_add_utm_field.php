<?php

class m140916_102908_add_utm_field extends CDbMigration
{
	public function up()
	{
		$um = UserModel::model();
		$md = $um->getMetaData();
		$db = $um->getDbConnection();

		if (!isset($md->columns['utm_source'])) {
			echo "Adding column " . $um->tableName() . ".utm_source ...\n";
			$db->createCommand()->addColumn($um->tableName(), 'utm_source', 'TEXT DEFAULT NULL');
		} else {
			echo "Column " . $um->tableName() . ".utm_source already exists ...\n";
		}
		
		if (!isset($md->columns['utm_campaign'])) {
			echo "Adding column " . $um->tableName() . ".utm_campaign ...\n";
			$db->createCommand()->addColumn($um->tableName(), 'utm_campaign', 'TEXT DEFAULT NULL');
		} else {
			echo "Column " . $um->tableName() . ".utm_campaign already exists ...\n";
		}

		if (!isset($md->columns['utm_medium'])) {
			echo "Adding column " . $um->tableName() . ".utm_medium ...\n";
			$db->createCommand()->addColumn($um->tableName(), 'utm_medium', 'TEXT DEFAULT NULL');
		} else {
			echo "Column " . $um->tableName() . ".utm_medium already exists ...\n";
		}
	}

	public function down()
	{
		$um = UserModel::model();
		$md = $um->getMetaData();
		$db = $um->getDbConnection();

		if (!isset($md->columns['utm_source'])) {
			echo "Removing column " . $um->tableName() . ".utm_source ...\n";
			$db->createCommand()->dropColumn($um->tableName(), 'utm_source');
		} else {
			echo "Column " . $um->tableName() . ".utm_source not exists ...\n";
		}
		
		if (!isset($md->columns['utm_campaign'])) {
			echo "Removing column " . $um->tableName() . ".utm_campaign ...\n";
			$db->createCommand()->dropColumn($um->tableName(), 'utm_campaign');
		} else {
			echo "Column " . $um->tableName() . ".utm_campaign not exists ...\n";
		}

		if (!isset($md->columns['utm_medium'])) {
			echo "Removing column " . $um->tableName() . ".utm_medium ...\n";
			$db->createCommand()->dropColumn($um->tableName(), 'utm_medium');
		} else {
			echo "Column " . $um->tableName() . ".utm_medium not exists ...\n";
		}
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
