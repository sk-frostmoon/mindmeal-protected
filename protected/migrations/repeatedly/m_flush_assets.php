<?php

class m_flush_assets extends CDbMigration
{
	public function up()
	{
        	exec('(cd ' . realpath(Yii::getPathOfAlias('application').'/../') . ' && [ -r assets ] && rm -rf assets/*)');
	}

	public function down()
	{
		echo get_called_class()." does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
