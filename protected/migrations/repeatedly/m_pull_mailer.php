<?php

class m_pull_mailer extends CDbMigration
{
	public function up()
	{
        	exec('(cd ' . Yii::getPathOfAlias('application.extensions.mailer') . ' && git pull origin master)');
	}

	public function down()
	{
		echo get_called_class()." does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
