<?php

class m_update_events extends CDbMigration
{
	public function up()
	{
		if (file_exists(Yii::getPathOfAlias('application.extensions.events'))) {
		    exec('(cd "' . Yii::getPathOfAlias('application.extensions.events') . '" && git pull origin master)');
		} else {
		    exec('(cd "' . Yii::getPathOfAlias('application.extensions') . '" && git clone gitrepo@dev.mindmeal.ru:repo/yii_extension_statEvents events)');
		    echo "\nWARN!! Check events component exists in config\n";
		}
	}

	public function down()
	{
		echo "m140827_101455_update_events does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
