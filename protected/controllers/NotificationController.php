<?php

class NotificationController extends Controller
{
    public $defaultAction = 'unsubscribe';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'users' => array('*'), 'actions' => array('unsubscribe')),
            array('allow', 'users' => array('@'), 'actions' => array('email', 'emailSubscribeAjax')),
            array('deny', 'users' => array('*')),
        );
    }

    // public actionEmail() {{{ 
    /**
     * actionEmail
     * 
     * @access public
     * @return void
     */
    public function actionEmail()
    {
        if (!isset(Yii::app()->mailer)) {
            $this->redirect($this->createUrl('/'));
        }

        $projects = Yii::app()->mailer->getProjectsList();

        $this->render('email', array('projects'=>$projects));
    }
    // }}}

    public function emailSubscribe($userEmail, $projectId, $templateId, $value)
    {
        $notification = NotificationModel::model()->findByAttributes(array('project_id' => $projectId, 'user_id' => $userEmail));
        if (is_null($notification)) {
            $notification = new NotificationModel();
            $notification->attributes = array(
                    'project_id' => $projectId,
                    'user_id'    => $userEmail,
            );
            if ($notification->validate()) {
                $notification->save(false);
            //} else {
                //Yii::log('error', CHtml::errorSummary($notification->getErrors()));
                //echo json_encode(array('debug' => CHtml::errorSummary($notification->getErrors())));
                //return $notification;
            }
        }

        if (!$notification->hasErrors()) {
            $notification->setTemplateSubscribe($templateId, $value);
            $notification->save();
        }

        return $notification;
    }
    
    // public actionEmailSubscribeAjax() {{{ 
    /**
     * actionEmailSubscribeAjax
     * 
     * @access public
     * @return void
     */
    public function actionEmailSubscribeAjax()
    {
        if (!Yii::app()->request->getIsAjaxRequest()) {
            $this->redirect($this->createUrl('/notification/email'));
            Yii::app()->end();
        }

        $projectId  = (int)Yii::app()->request->getParam('project_id', false);
        $templateId = Yii::app()->request->getParam('template_id', 0);
        $value      = (Yii::app()->request->getParam('value', 1)==1) ? true : false;

        if ($projectId === false) {
            echo json_encode(array('success' => false));
            Yii::app()->end();
        }

        $notification = $this->emailSubscribe(Yii::app()->user->email, $projectId, $templateId, $value);
        if ($notification !== NULL) { 
            echo json_encode(array('errors' => $notification->getErrors(), 'success'=>!$notification->hasErrors()));
        } else {
            echo json_encode(array('errors' => array('project_id' => 'Unknown error catched', 'success'=>false)));
        }
    }
    // }}}

    public function actionUnsubscribe()
    {
        if (Yii::app()->user->getIsGuest()) {
            $this->redirect($this->createAbsoluteUrl('/user/login', array('url' => $this->createUrl('/notification/unsubscribe'))));
        }
        #$email   = Yii::app()->request->getParam('email', false);
        $confirm = Yii::app()->request->getParam('confirm', false);

        if (($confirm !== false)) {
            // projectId=0 - all products
            $notification = $this->emailSubscribe(Yii::app()->user->email, 0, 0, 0);
            if ($notification && !$notification->hasErrors()) {
                $this->render('unsubscribe_success');
                Yii::app()->end();
            }
        }

        $this->render('unsubscribe', array('hasErrors' => (isset($notification) && $notification) ? $notification->hasErrors() : false));
    }
}
