<?php

class UserController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users
                'actions' => array('index', 'login', 'success', 'license', 'panel', 'logout', 'restore', 'forgotten', 'register', 'activate',
                                    'getFDHappinessKey'),
                'users'   => array('*'),
            ),
            array('allow',  // allow registered users
                'actions' => array('profile', 'edit', 'uploadResized', 'registerGreetings'),
                'users'   => array('@'),
            ),
            array('allow',
                'actions' => array('resttest', 'emailtest'),
                'ips' => array('127.0.0.1'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function init()
    {
        parent::init();
    }

    // public actionIndex() {{{ 
    /**
     * actionIndex
     * 
     * @access public
     * @return void
     */
    public function actionIndex()
    {
        if (Yii::app()->request->getParam('token') && Yii::app()->request->getParam('host')) {
            Yii::app()->session['need_site_auth'] = array(
                'token' => Yii::app()->request->getParam('token'),
                'host' => Yii::app()->request->getParam('host')
            );
        }

        if (Yii::app()->user->isGuest) {
            $this->redirect($this->createUrl('/user/login'));
        } else {
            $userModel = new UserModel();
            $userModel->authHost();
        }


        $this->redirect($this->createUrl('/user/profile'));
        //$this->render('index');
    }
    // }}}

    // public actionProfile() {{{ 
    /**
     * actionProfile
     * 
     * @access public
     * @return void
     */
    public function actionProfile()
    {
        if (Yii::app()->user->getIsGuest()) {
            $this->redirect($this->createUrl('/user/logout'));
            return;
        }

	$user = UserModel::model()->findByPk(Yii::app()->user->id);
        if (isset($_POST['UserModel'])) {
            $user->setScenario('changePassword');
            $user->attributes = $_POST['UserModel'];
            if ($user->validate() && $user->save(array('password'))) {
                Yii::app()->user->setFlash('notice', Yii::t('secure', 'Password changed successfully'));
                $this->redirect($this->createUrl('/user/profile'));
            } else {
                Yii::app()->user->setFlash('error', CHtml::errorSummary($user));
            }
        }

        $this->render('profile', array('user'=>$user, 'token' => $user->restfullToken));
    }
    // }}}

    // public actionEdit() {{{ 
    /**
     * actionEdit
     * 
     * @access public
     * @return void
     */
    public function actionEdit()
    {
        if (Yii::app()->user->getIsGuest()) {
            $this->redirect('/user/logout');
            return;
        }

	$model = UserModel::model()->findByPk(Yii::app()->user->id);
        $uri   = Yii::app()->request->getParam('url', $this->createUrl('user/profile'));

        if (isset($_POST['Password'])) {
            $model->setScenario('changePassword');
            //$model->attributes = $_POST['Password'];
            $model->attributes = Yii::app()->request->getParam('Password');

            if ($model->validate()) {
                $model->save(false, array('password', 'updated'));
                $model->refresh();

                Yii::app()->user->onUserPasswordChanged(new CEvent($this, array('user' => $model)));

                $token = RestfullToken::findByEmail($model->email);
                if ($token != false) {
                    $token->byebye();
                }

                //@sk change Solgame password Too
                $sol = new SolGame();
                $sol->changePassword();

                Yii::app()->user->setFlash('notice', 'Пароль успешно изменен');
            }
        }

        $this->render('edit', array('model' => $model));
    }
    // }}}

    // public actionPanel() {{{ 
    /**
     * actionPanel
     * 
     * @access public
     * @return void
     */
    public function actionPanel()
    {
        $user = UserModel::model()->findByPk(Yii::app()->user->id);
        $this->renderPartial('panel', array('user'=>$user));
    }
    // }}}


    // public actionUploadCrop() {{{ 
    /**
     * actionUploadCrop
     * 
     * @access public
     * @return void
     */
    public function actionUploadCrop()
    {
        Yii::app()->user->setFlash('notice', 'Sorry, we make autocrop some later');
        $this->redirect($this->createUrl('/user/profile'));
    }
    // }}}

    // public actionUploadResized() {{{ 
    /**
     * actionUploadResized
     * Download image that cropped to 200x200 in base64
     * @post string image
     *
     * @http_response_code 411 No image in POST
     * @http_response_code 415 Received image is not 'image/png' mime or/and not 200x200 px in dimensions
     * @http_response_code 422 Error in base64 decoding image process
     * @access public
     * @return void
     */
    public function actionUploadResized()
    {
        if (Yii::app()->user->isGuest) {
            $this->redirect($this->createUrl('/user/login'));
            return;
        }

        $result = array();

        $image = Yii::app()->request->getParam('image', false);
        if ($image !== false) {
            $pos = strpos($image, ',');
            if (($pos != false) && (strcmp(substr($image, 0, $pos), 'data:image/png;base64') === 0)) {
                $image = substr($image, $pos+1);
                $image = base64_decode($image);
                if (($image !== false) && !empty($image)) {
                    
                    $tempFile = tempnam(Yii::getPathOfAlias('webroot') . "/images/avatars/", ".upload");
                    file_put_contents($tempFile, $image);
                    $size = getimagesize($tempFile);
                    if (($size === false) || ($size[0] != 200) || ($size[1] != 200) || ($size['mime'] != 'image/png')) {
                        http_response_code(415); // @ak 415 Unsupported Media Type
                        $result['error'] = array('message' => 'Изображение не ссответствует стандарту image/png 200x200');
                    } else {
                        $avatarFile = Yii::getPathOfAlias('webroot') . "/images/avatars/" . Yii::app()->user->id . ".png";
                        if (file_exists($avatarFile)) {
                            unlink($avatarFile);
                        }
                        rename($tempFile, $avatarFile);
                    }
                } else {
                    http_response_code(422); // @ak 422 Unprocessable Entity
                    $result['error'] = array('message' => 'Некорректный формат входящих данных');
                }
            } else {
                http_response_code(422); // @ak 422 Unprocessable Entity
                $result['error'] = array('message' => 'Некорректный формат входящих данных');
            }
        } else {
            http_reponse_code(411); // @ak 411 Length Required
            $result['error'] = array('message' => 'Некорректный формат входящих данных');

        }

        echo json_encode($result);
    }
    // }}}

    // public actionLogin() {{{ 
    /**
     * actionLogin
     * 
     * @access public
     * @return void
     */
    public function actionLogin()
    {
        $this->layout = "small";
        $url = Yii::app()->request->getParam('url', false);

        if (!Yii::app()->user->getIsGuest()) {
            $this->redirect($url ? $url : $this->createUrl('user/profile'));
            Yii::app()->end();
        }

        $model = new LoginForm('login');

        if (isset($_POST['LoginForm']))
        {
            $model->attributes = $_POST['LoginForm'];

            if ($model->validate() && $model->login()) {
                $this->addStatisticCookie('stat_push', 'user_login', array('method' => 'direct'));
                $this->redirect(($url !== false) ? $url : $this->createUrl('user/profile'));
            }
        }

        $this->render('login', array('model'=>$model, 'url' => $url));
    }
    // }}}

    // public actionLogout() {{{ 
    /**
     * actionLogout
     * 
     * @access public
     * @return void
     */
    public function actionLogout()
    {
        $url = Yii::app()->request->getParam('url', false);

        $userModel = new UserModel();
        $userModel->logoutHosts();

        Yii::app()->user->logout();

        $this->redirect($this->createUrl('user/login'));
    }
    // }}}

    // public actionForgotten() {{{ 
    /**
     * actionForgotten
     * 
     * @access public
     * @return void
     */
    public function actionForgotten()
    {
        if ( ! Yii::app()->user->getIsGuest() ) {
            $this->redirect('/user/edit');
            return;
        }

        $this->layout = "small";

        $model = new ForgottenForm();

        if (isset($_POST['ForgottenForm'])) {
            $model->attributes = Yii::app()->request->getParam('ForgottenForm');
            if ($model->validate() && $model->mail()) {
                Yii::app()->user->setState('success_view', 'forgotten_sended');
                $this->redirect('/user/success');
            }
        }

        $this->render('forgotten', array('model'=>$model));
    }
    // }}}

    // public actionRestore() {{{ 
    /**
     * actionRestore
     * After action Forgotten 
     * 
     * @access public
     * @return void
     */
    public function actionRestore()
    {
        if ( ! Yii::app()->user->getIsGuest() ) {
            $this->redirect('/user/edit');
            return;
        }

        $this->layout = "small";

        $token = Yii::app()->request->getParam('token', NULL);

        $model = UserModel::model()->findByToken($token);
        if (!is_null($model)) {
            $model->setScenario('restore');

            if (isset($_POST['UserModel'])) {
                $model->attributes = Yii::app()->request->getParam('UserModel');

                if ($model->validate()) {
                    $model->save(false, array('password', 'updated'));
                    $model->refresh();

                    Yii::app()->user->onUserPasswordChanged(new CEvent($this, array('user' => $model)));

                    $token = RestfullToken::findByEmail($model->email);
                    if ($token != false) {
                        $token->byebye();
                    }

                    //@sk change Solgame password Too
                    $sol = new SolGame();
                    $sol->changePassword($model->id);

                    Yii::app()->user->setState('success_view', 'restore_success');
                    $this->redirect('/user/success');
                }
            }
        } else {
            $userToken = '';
            $model = new UserModel('restore');
        }

        $this->render('restore', array('model' => $model));
    }
    // }}}

    // public actionSuccess() {{{ 
    /**
     * actionSuccess
     * 
     * @access public
     * @return void
     */
    public function actionSuccess()
    {
        if (Yii::app()->user->getIsGuest()) {
            $this->layout = "small";
        }

        $view = Yii::app()->user->getState('success_view');
        if ($view == NULL) {
            $this->redirect('/user/login');
        } else {
            Yii::app()->user->setState('success_view', NULL);
            $this->render($view);
        }
    }
    // }}}

    // public actionRegister() {{{ 
    /**
     * actionRegister
     * 
     * @access public
     * @return void
     */
    public function actionRegister()
    {
        $this->layout = "small";
        $url  = Yii::app()->request->getParam('url', false);
        $host = '/';
        if ($url !== false) {
            if ((strpos($url, 'http://') === 0) || (strpos($url, 'https://') === 0)) {
                $host = preg_replace('/^(https?:\/\/[^\/]+)(|\/.*)$/', '\1', $url);
            }
        }
        $model = new UserModel('register');
        $model->host = $host;

        if (isset($_POST['UserModel'])) {
            $attributes = Yii::app()->request->getParam('UserModel');
            foreach (Yii::app()->events->getUtmData() as $key=>$value) {
                $attributes[$key] = $value;
            }
            $model->attributes = $attributes;

            if ($model->validate()) {
                $model->save(false);

                $model = UserModel::model()->findByPk($model->id); // @ak: Important to update UserModel::$_token

                $model->setScenario('activation');
                $model->attributes = array(
                        'activated' => 1,
                );
                $model->save(false, array('activated'));
                $model->refresh();

                Yii::app()->user->setCustom($model);

                $UserProfileModel = new UserProfileApiModel($model);

                Yii::app()->user->onUserRegister(new CEvent($this, array(
                        'method' => 'direct',
                        'user'   => $UserProfileModel,
			'host'   => $host,
                )));

                // @ak: refs #430 Second registration
                //Yii::app()->user->logout();

		if (Yii::app()->user->getIsGuest()) {
			$loginForm = new LoginForm('login');
			$loginForm->attributes = $attributes;

			$model->host = $host; // @ak: When refreshed - model doesn't have a host value
			$this->render('register_greetings', array(
			    'model' => $loginForm,
			    'user'  => $model,
			    'url'   => $url,
			));
		} else {
			$this->redirect($this->createUrl('/user/profile'));
		}
                Yii::app()->end();
            }
        }

        $this->render('register', array('model' => $model, 'url' => $url));
    }
    // }}}

    // public actionActivate() {{{ 
    /**
     * actionActivate
     * User call this action after click on link from email
     * 
     * @access public
     * @return void
     */
    public function actionActivate()
    {
        $token = Yii::app()->request->getParam('token', NULL);

        $model = UserModel::model()->findByToken($token);
        if (!is_null($model)) {
            if ($model->getIsActivated()) {
                $model->addError('activated', 'Account was already activated.');
            } else {
                $model->setScenario('activation');
                $model->attributes = array(
                        'activated' => 1,
                );
                $model->save(false, array('activated'));
                $model->refresh();

                /* @ak: TODO: No template for notify user when account is activated
                Yii::app()->mailer->postRenderPublish($model->email, SecureProject::TPL_ACTIVATE, array(
                    'activateUrl'      => CHtml::encode( Yii::app()->createAbsoluteUrl('/user/activate', array('token' => $model->getToken())) ),
                    'activateUrlLabel' => CHtml::encode( Yii::app()->createAbsoluteUrl('/user/activate', array('token' => $model->getToken())) ),
                ));
                */
            }
        } else {
            $model = new UserModel();
            if ($token === NULL) {
                $this->redirect('/user/login');
            } else {
                $model->addError('activated', 'Token has been expired');
            }
        }

        $this->render('activation', array('model' => $model));
    }
    // }}}

    // public actionLicense() {{{ 
    /**
     * actionLicense
     * 
     * @access public
     * @return void
     */
    public function actionLicense()
    {
        $this->layout = "small";
        $this->renderPartial('license_agreement');
    }
    // }}}

    // public actionRestTest() {{{ 
    /**
     * actionRestTest
     * 
     * @access public
     * @return void
     */
    public function actionRestTest()
    {
        $this->render('application.views.rest.test_api');
    }
    // }}}

    public function actionEmailTest()
    {
        $params = $_GET;
        $email      = isset($params['email'])      ? $params['email']      : false;
        $templateId = isset($params['templateId']) ? $params['templateId'] : false;

        if (($templateId === false) || ($email === false)) {
            Yii::app()->end();
        }

        unset($params['email']);     // = null;
        unset($params['templateId']);// = null;

        Yii::import('ext.mailer.models.' . Yii::app()->mailer->projectClass, true);

        if (defined($templateId)) {
            $templateId = constant($templateId);
        } else if (defined(Yii::app()->mailer->projectClass . "::" . $templateId)) {
            $templateId = constant(Yii::app()->mailer->projectClass . "::" . $templateId);
        } else {
            $templateId = (int)$templateId;
        }
        //var_dump($templateId, constant('SecureProject::TPL_SOLGAME_INVITE_ACTIVATE'));
        //die(__METHOD__);

        Yii::app()->mailer->postRenderPublish($email, $templateId, $params);
    }


    public function actionGetFDHappinessKey()
    {

        header('Content-Type: application/json');
        $MAXIMUM_FD_KEYS = 1809;
        $projectName = 'fd_happiness_key_2609';
        $keyscount = Yii::app()->db->createCommand('SELECT count(*) FROM invite_key WHERE project = "'.$projectName.'"')->queryScalar();

        $result = array(
            'project' => array(
                'id'    => 0,
                'key'   => 'xxxx-xxxx-xxxx-xxxx',
                'left'  => max($MAXIMUM_FD_KEYS-$keyscount, 0),
                'total' => $MAXIMUM_FD_KEYS,
            ),
        );
	Yii::log(__METHOD__.' : Count keys: ' . ($MAXIMUM_FD_KEYS-$keyscount), 'error');

	echo json_encode($result);
	return;

        if (Yii::app()->request->getParam('id', false)) {
            $key = InviteModel::model()->findByPk(Yii::app()->request->getParam('id', false));
            if ($key !== NULL) {
                $result['project']['id']  = $key->id;    
                $result['project']['key'] = $key->value;    
            } else {
                $result['project']['key'] = 'CANT-FIND-YOUR-KEY0';    
            }
        } else {
	    if ($result['project']['left'] > 0) {
		    $key = Yii::app()->db->createCommand('SELECT serial FROM vwRandomKeyFormatted')->queryScalar();
		    $invite = Yii::app()->db->createCommand('INSERT INTO invite_key (value, project) VALUES (:value, :project)');
		    $isSaved = $invite->execute(array(
			':value'   => $key,
			':project' => $projectName,
		    ));
		    if ($isSaved) {
			$result['project']['id']    = Yii::app()->db->getLastInsertId();
			$result['project']['key']   = $key;
			$result['project']['left'] -= 1;
		    } else {
			$result['project']['key'] = 'ERRO-RWHI-LEGE-NERA';
			$result['error'] = $invite->getErrors();
		    }
	    }
        }
        echo json_encode($result);
    }
}

// vim: ts=4 sw=4 et foldmethod=indent foldlevel=1
