<?php

class InviteController extends Controller
{

    public function actionIndex()
    {
        $this->redirect("/user/profile/");
    }


    /**
     * Это временный контроллер для создания учетки
     */
    public function actionSolgame(){

        //@ak: disabled, will be removed later
        $this->redirect('/user/profile');
        // END


        //@sk old check on auth
        if (Yii::app()->user->isGuest) {
            $this->redirect('/user/login/');
            return;
        }
        $user = UserModel::model()->findByPk(Yii::app()->user->id);
        if (is_null($user)) {
            $this->redirect('/user/logout');
            return;
        }

        $result = false;
        $sg = null;

        if (!empty($_POST['InviteModel']['value'])) {
            $model = InviteModel::model()->findByCode($_POST['InviteModel']['value']);

            if (is_null($model)) {
                $model = new InviteModel();
                $model->addError('value', "Неверный код!");
            }
        } else {
            $model = new InviteModel();
        }


        if (!$model->getIsNewRecord()) {
            $model->owner = Yii::app()->user->id;
            $model->active = 1;
            $model->active_date = new CDbExpression('CURRENT_TIMESTAMP');
            $model->save();

            if (!$model->hasErrors()) {
                //@sk
                try {
                    Yii::app()->sol_game_db;
                    $user = UserModel::model()->findByPk(Yii::app()->user->id);

                    $sg = new SolGame();
                    if (isset(Yii::app()->events)) {
                        $sg->onAccountCreated = array('StatEvents', 'onInviteActivated');
                    }
                    $sg->createAccount();
                    if (!$sg->hasErrors()) {
                        Yii::app()->mailer->postRenderPublish($user->email, SecureProject::TPL_SOLGAME_INVITE_ACTIVATE, array(
                            'userLogin' => $user->username,
                        ));
                        $result = true;
                    }

                } catch(CDbException $e) {
                    $model->addError("value", 'Создание учетной записи для SolGame сейчас невозможно. Обратитесь в службу поддержки.');
                    Yii::log($e->getMessage(), 'sol_game_db');
                }
            }
        }

        $this->render('index', array('model' => $model, 'user' => $user, 'result' => $result, 'sg' => $sg));

    }

    public function actionFd()
    {


    }
}
