<?php

/**
 * RestController Информация об API 
 * @autogen_api
 * @wiki_uri Autogen_API
 * @additional {{{
 * Общая информация о запросах:
 * * Запросы происходят по стандарту HTTP/HTTPS на домен http://login.mindmeal.ru
 * * Запросы возвращают JSON в теле документа.
 * * Запрос возвращает код ответа _200 OK_ (успешен и возвращает данные) или _401 Unauthorized_ (не авторизован)
 * * Необходимость авторизации по [[Внутренний_стандарт_HTTP-WSSE_запроса|внутреннему стандарту WSSE]] указана в описании АPI
 * * Параметры передаваемые серверу отправляются в теле POST-запроса либо в URI GET-запроса
 * }}}
 * 
 * @uses Controller
 * @package 
 * @version $id$
 * @copyright 2006-2009 FanIQ
 * @author kevin olson <acidjazz@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 */
class RestController extends Controller
{
    public function render($action, $data)
    {
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode(array(
                $action => $data
        ));
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        if (defined('IS_REST_REQUEST') && IS_REST_REQUEST) {
            return array(
                array('allow', 'users' => array('*'), 'actions' => array('echo', 'login', 'register', 'getPublicInfo', 'getOnlineUsers')),
                array('allow', 'users' => array('@'), 'actions' => array(
                        'alive', 'logout', 'play', 'isOnline', 'test',
                        'getProfile',
                        'clientReady', 'clientPlay', 'clientClosed',
                        'setState', 'getState',
                        'getStatus', 'setStatus',
                    )
                ),
                array('deny',  'users' => array('*')),
            );
        } else {
            return array(
                array('allow', 'actions' => array('echo'), 'users' => array('*')),
                array('deny', 'users' => array('*')),
            );
        }
    }

    // public function beforeAction() set the protected $rtoken = Yii::app()->user->getRestfullToken();

    // public actionLogin() {{{
    /**
     * actionLogin Авторизация пользователя
     * Проверка полученных логина(EMail) и пароля пользователя.
     * В случае получения корректных данных возвращяется token для аутентификации по WSSE.
     * @uri /api/user/login
     * @post string auth_user Email пользователя
     * @post string auth_pw Пароль пользоваеля
     *
     * @http_response_code 200 Авторазационные данные верны. Возвращается токен.
     * @http_response_code 401 Указаны некорректные данные авторизации.
     * @access public
     * @return void
     * @example JSON {{{
     * {
     *   "token":"abcd1234abcd1234abcd1234abcd1234",
     * }
     * }}}
     */
    public function actionLogin()
    {
        $this->render('token', Yii::app()->user->getToken());
    }
    // }}}

    // public actionRegister() {{{ 
    /**
     * actionRegister
     * Регистрация пользователя с помощью удаленной формы регистрации
     * @uri /api/user/register
     * 
     * @http_response_code 404 Хост вне списка доверенных
     * @http_response_code 400 Ошибка валидации запроса
     * @http_response_code 412 При валидации модели возникли ошибки
     * @post UserModel[username]        Имя пользователя
     * @post UserModel[email]           Email пользователя
     * @post UserModel[password]        Пароль
     * @post UserModel[passwordConfirm] Повтор пароля
     * @post UserModel[host]            Хост запроса
     * @post UserModel[crc]             substr( sha1( http_build_query(array('UserModel' => $_POST['UserModel'])) . $salts[$_POST['UserModel']['host']] ), 0, 12 )
     * @access public
     * @return void
     */
    public function actionRegister()
    {
        $model = new RestUserModel('register');
        if (isset($_POST['RestUserModel'])) {
            $attributes = Yii::app()->request->getParam('RestUserModel');
            foreach (Yii::app()->events->getUtmData() as $key=>$value) {
                $attributes[$key] = $value;
            }
            $model->attributes = $attributes;

            if ($model->validate()) {
                $model->save();

                $model = UserModel::model()->findByPk($model->id); // @ak: Important to update UserModel::$_token

                $model->setScenario('activation');
                $model->attributes = array(
                        'activated' => 1,
                );
                $model->save(false, array('activated'));
                $model->refresh();

                $UserProfileModel = new UserProfileApiModel($model);

                Yii::app()->user->onUserRegister(new CEvent($this, array(
                        'method' => 'direct',
                        'user'   => $UserProfileModel,
                        'host'   => (isset($attributes['host']) ? $attributes['host'] : '-'),
                )));

                $this->render('success', array('userId'=>$model->id));
                Yii::app()->end();
                /*
                $loginForm = new LoginForm('login');
                $loginForm->attributes = $attributes;

                if ($loginForm->validate() and $loginForm->login()) {
                    Yii::app()->session['__user:firstLogin'] = array(
                            'example_item' => rand(1000, 9999),
                    );
                    //$this->redirect(
                    //    $this->createUrl('/user/registerGreetings', array(
                    //        'url' => $url ? $url : $this->createUrl('user/profile'),
                    //    ))
                    //);
                    //Yii::app()->end();
                }
                */
            } else {
                $code = $model->getError('host_code');
                http_response_code( ($code == NULL) ? $code : 412 );
                $this->render('errors', $model->getErrors());
                Yii::app()->end();
            }
        } else {
            //$this->render('register', array('model' => $model, 'url' => $url));
            $this->render('error', array('message' => 'No arguments'));
        }
    }
    // }}}

    public function actionGetUnsibscribeList()
    {
        $projectId = Yii::app()->request->getParam('projectId', false);
        $result = array(
                'project' => false,
                'templates' => array(),
        );
        if ($projectId !== false) {
            $projects = Yii::app()->mailer->getProjectsList();
            if (isset($projects[$projectId])) {
            }
        }
        $this->render('unsubscribe', $result);
    }

    // public actionLogout() {{{
    /**
     * actionLogout Завершение сессии
     * @wsse
     * @uri /api/user/logout
     * 
     * @http_response_code 410 Пользователь уже разлогинен
     * @access public
     * @return void
     */
    public function actionLogout()
    {
        if (!Yii::app()->user->logout()) {
            http_response_code(410);
        }
    }
    // }}}

    // public actionTest() {{{ 
    /**
     * actionTest Тест-запрос
     * @deprecated Используйте /api/user/isOnline
     * @wsse
     * @uri /api/user/test
     * 
     * @access public
     * @return void
     */
    public function actionTest()
    {
        $this->render('test', true);
    }
    // }}}

    // public actionIsOnline() {{{ 
    /**
     * actionIsOnline Проверка пользователя на присутствие в онлайн
     * @wsse
     * @uri /api/user/isOnline
     *
     * @http_response_code 404 Состояние пользователе не "online"("Онлайн")
     * @access public
     * @return void
     */
    public function actionIsOnline()
    {
        $token = Yii::app()->user->getRestfullToken();
        $isOnline = (strcmp($token->state, RestfullToken::STATE_ONLINE) === 0);
        if (!$isOnline) {
            http_response_code(404);
        }
    }
    // }}}

    public function actionIsReadyToGame()
    {
        $token = Yii::app()->user->getRestfullToken();
        $isReady = (strcmp($token->state, RestfullToken::STATE_ONLINE) === 0) && (strcmp($token->status, RestfullToken::STATUS_WAITING_QUEUE) === 0);
        if (!$isReaddy) {
            http_response_code(424);
        }
    }

    // public actionAlive() {{{
    /**
     * actionAlive Ping-запрос
     * Обновление времени статуса онлайн текущего пользователя
     * В случае состояния пользователя "offline"("Оффлайн") пользователь переводится в состояние "in_launcher"("В лаунчере")
     * В случае вызова чаще чем раз в 30 секунд принимается что лаунчер и клиент игры запущены,
     * в последствии это влияет на выставление состояния пользователя после закрытия клиента игры (см. /api/user/clientClosed).
     * TODO:
     * * @post string clientid Client id for checking a unique login from account(forming by launcher)
     * For Debug use clientid = deadbeefdeadbeefdeadbeefdeadbeef
     * @wsse
     * @post string app Уникальный идентификатор приложения
     * @uri /api/user/alive
     * 
     * @http_response_code 403 Ошибка возникает в случае уже запущенного другого приложения
     * @access public
     * @return void
     */
    public function actionAlive()
    {
        $app      = Yii::app()->request->getParam('app',      false);
        $clientId = Yii::app()->request->getParam('clientid', 0);

        $token    = Yii::app()->user->getRestfullToken();
        $appReady = true;

        if ($app != $token->getApplication()) {
            $appReady = ($token->setApplication($app) && $token->setUniqueId($clientId));
            if ($appReady) {
                $token->save();
            } else {
                http_response_code(403);
            }
        } else {
            // TODO: Check client unique id
            $token->touchAlive();
        }
    }
    // }}}

    // public actionEcho() {{{
    /**
     * actionEcho Эхо-запрос
     * Возвращает все пришедшие аргументы в виде JSON-объекта
     * @uri /api/user/echo
     * 
     * @access public
     * @return void
     */
    public function actionEcho()
    {
        $this->render('echo', $_REQUEST);
    }
    // }}}

    // public actionGetProfile() {{{
    /**
     * actionGetProfile Получение подробной информации о текущем пользователе
     * @wsse
     * @uri /api/user/getProfile
     * 
     * @access public
     * @return void
     * @example JSON {{{
     * {
     *   "profile": {
     *     "id":7,
     *     "avatar":"http://login.mindmeal.ru/images/avatars/7",
     *     "level":5,
     *     "state":"online",
     *     "status":"lobby",
     *     "username":"User NickName",
     *     "bdate":550516400,
     *     "created":1395139360,
     *     "fname":"",
     *     "lname":"",
     *     "access":{
     *         "fd":0,
     *         "solgame":0,
     *     }
     *   }
     * }
     * }}}
     */
    public function actionGetProfile()
    {
        $user  = Yii::app()->user->getUserData();
	// @ak temporary disabled
        //$token = Yii::app()->user->getRestfullToken();

        $gameAccess = array();

        $gameAccountsModel = new GameAccountsModel();

	//@sk check
	if(strpos($user->email, "@mindmeal.ru") !== false){
		$gameAccess['fd'] = 1;
	} else {
		$gameAccess['fd'] = 0; //(int)$gameAccountsModel->checkAccountByName("finaldesire", Yii::app()->user->id);
	}

        $gameAccess['solgame']  = $gameAccountsModel->checkAccountByName("solgame", Yii::app()->user->id);

        $this->render('profile', array(
                        'id' => $user->id,
                        'avatar' => $user->getAvatarUrl(),
                        'level'  => 1, //$token->level,
                        'state'  => RestfullToken::STATE_ONLINE, //$token->getState(),
                        'status'  => RestfullToken::STATUS_WAITING_QUEUE, //$token->getStatus(),
                        'username' => $user->username,
                        'bdate'    => $user->bdate,
                        'created'  => $user->created,
                        'fname'    => $user->fname,
                        'lname'    => $user->lname,
                        'accesses' => $gameAccess,
                        //'isTwoPingers' => (int)$token->checkBothPingers(),
                )
        );
    }
    // }}}

    // public actionSetStatus() {{{
    /**
     * actionSetStatus Установка статуса пользователя находящегося в онлайн
     * NOTE: В дальнейшем вызов setStatus должен происходить только с доверенных серверов.
     * @wsse
     * @uri /api/user/setStatus
     * @post string status Cтатус пользователя. Принимаемые значения (разделены |): {@enum RestfullToken::STATUS_*}
     * 
     * @http_response_code 403 Установка статуса невозможна.
     * @access public
     * @return void
     */
    public function actionSetStatus()
    {
        $status = Yii::app()->request->getParam('status',  false);
        $token  = Yii::app()->user->getRestfullToken();

        if ($token->setStatus($status) && $token->save()) {
            return;
        }

        http_response_code(403);
    }
    // }}}

    // public actionClientReady() {{{ 
    /**
     * actionClientReady Событие о запуске клиента игры
     * Событие отправляется единоразово при запуске клиента игры.
     * Пользователь переводится из состояния "В лаунчере" в состояние "Онлайн" и получает статус "lobby"("В лобби").
     * @wsse
     * @uri /api/user/clientReady
     * 
     * @http_response_code 403 Невозможно перевести пользователя в состояние "Онлайн"
     * @access public
     * @return void
     */
    public function actionClientReady()
    {
        $token = Yii::app()->user->getRestfullToken();
        if ($token->setState(RestfullToken::STATE_ONLINE) && $token->save()) {
            return; // ok
        }

        // Here stop the specific 
        http_response_code(403);
    }
    // }}}

    // public actionClientPlay() {{{ 
    /**
     * actionClientPlay
     * Событие отправляется при нажатии кнопки Play в игровом интерфейсе.
     * ИЛИ: Событие отправляется сервером при постановке пользователя в очередь.
     * Пользователь получает статус "waiting_queue"("В очереди"). 
     * @wsse
     * @uri /api/user/clientPlay
     *
     * @http_response_code 403 Пользователь не в онлайн либо невозможно установить пользователю статус "waiting_queue"
     * @access public
     * @return void
     */
    public function actionClientPlay()
    {
        $token = Yii::app()->user->getRestfullToken();
        if (strcmp($token->state, RestfullToken::STATE_ONLINE) === 0) {
            if ($token->setStatus(RestfullToken::STATUS_WAITING_QUEUE) && $token->save()) {
                return; // ok
            }
        }
        http_response_code(403);
    }
    // }}}

    // public actionClientClosed() {{{ 
    /**
     * actionClientClosed Событие о закрытии клиента игры
     * Запрос отправляется клиентом игры при закрытии.
     * Пользователь переводится в состояние "В лаунчере" при запущенном лаунчере,
     * либо в состояние "Оффлайн" при закрытом лаунчере.
     * @wsse
     * @uri /api/user/clientClosed
     * 
     * @access public
     * @return void
     */
    public function actionClientClosed()
    {
        $token = Yii::app()->user->getRestfullToken();
        if (strcmp($token->state, RestfullToken::STATE_ONLINE) === 0) {
            $state = ($token->checkBothPingers() ? RestfullToken::STATE_LAUNCHER : RestfullToken::STATE_OFFLINE);
            if ($token->setState($state) && $token->save()) {
                return; // ok
            }
        }
        http_response_code(403);
    }
    // }}}

    public function actionGetState()
    {
        $this->render('getState', Yii::app()->user->getRestfullToken()->getState());
    }
    public function actionGetStatus()
    {
        $this->render('getStatus', Yii::app()->user->getRestfullToken()->getStatus());
    }

    // public actionGetPublicInfo() {{{
    /**
     * actionGetPublicInfo Получение публичной информации о любом пользователе или пользователях в онлайне
     * Вызывается клиентом для получения краткой информации об игроках в сессии
     * @wsse
     * @uri /api/user/getPublicInfo
     * @get integer|integer[] id    ID или массив ID пользователей информацию о которых необходимо получить
     * 
     * @access public
     * @return void
     * @example JSON {{{
     * {
     *   "users":[
     *     {"id":1,"username":"User NickName","avatar":"http://login.mindmeal.ru/avatars/1","level":3,"state":"online"},
     *     {"id":2,"username":"User NickName","avatar":"http://login.mindmeal.ru/avatars/2","level":8,"state":"online"}
     *   ],
     * }
     * }}}
     */
    public function actionGetPublicInfo()
    {
        $userIds = Yii::app()->request->getParam('id', array());
        if (!is_array($userIds)) {
            $userIds = array($userIds);
        }

        $userIds = array_flip($userIds);
        $data    = array();
        $tc      = Yii::app()->tokenCache;
        foreach (RestfullToken::getAllKeys() as $key) {
            $token = $tc->get($key);
            if ($token && $token->user && isset($userIds[$token->user->id])) {
                $user = $token->user;
                $data[] = array(
                        'id'     => $user->id,
                        'username' => $user->username,
                        'avatar' => $user->getAvatarUrl(),
                        'level'  => $token->level,
                        'state'  => $token->getState(),
                );
            }
        }

        $this->render('users', $data);
    }
    // }}}

    
    // public actionGetOnlineUsers() {{{ 
    /**
     * actionGetOnlineUsers Получение списка всех пользователей в онлайне
     * *используется только для статистики*
     * Возвращается массив из ID и username пользователей в онлайн.
     * Пользователь будет отвутствовать в списке, если он не в состоянии "Онлайн"
     * @uri /api/user/getOnlineUsers
     * 
     * @access public
     * @return void
     * @example JSON {{{
     * {
     *   "users":[
     *     {"id":1,"username":"User1 NickName"},
     *     {"id":2,"username":"User2 NickName"},
     *   ],
     * }
     * }}}
     */
    public function actionGetOnlineUsers()
    {
        $tc = Yii::app()->tokenCache;
        $data = array();
        foreach (RestfullToken::getAllKeys() as $key) {
            $token = $tc->get($key);
            $user  = $token->user;
            $data[] = array(
                    'id' => $user->id,
                    'username' => $user->username,
            );
        }

        $this->render('users', $data);
    }
    // }}}
}
