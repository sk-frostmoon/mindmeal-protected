<?php

class FdController extends Controller
{

    /*
     * Состояние учетной записи (есть или нет)
     */
    public function actionIndex()
    {
        //@sk old check on auth
        if (Yii::app()->user->isGuest) {
            $this->redirect('/user/login/');
            return;
        }
        $user = UserModel::model()->findByPk(Yii::app()->user->id);
        if (is_null($user)) {
            $this->redirect('/user/logout');
            return;
        }

        //@sk need check isset solgame account
        $gameAccountsModel = new GameAccountsModel();
        $haveAccount = $gameAccountsModel->checkAccountByName("finaldesire", Yii::app()->user->id);
        if(!$haveAccount){
            $this->redirect('/fd/create');
        }

        $this->render("index", array("user" => $user, "haveAccount" => $haveAccount));
    }

    /*
     * Создаем учетную запись
     */
    public function actionCreate()
    {
        //@sk old check on auth
        if (Yii::app()->user->isGuest) {
            if(isset($_GET['key'])){
		$this->redirect('/user/login/?url='. urlencode('/fd/create/?key='. $_GET['key']));
            } else {
                $this->redirect('/user/login/');
            }
            return;
        }
        $user = UserModel::model()->findByPk(Yii::app()->user->id);
        if (is_null($user)) {
            $this->redirect('/user/logout');
            return;
        }

        //@sk need check isset fd account
        $gameAccountsModel = new GameAccountsModel();
        $haveAccount = $gameAccountsModel->checkAccountByName("finaldesire", Yii::app()->user->id);
        $result = false;
        $errors = null;

        if($haveAccount){
            $this->redirect('/fd/');
            return;
        }

        $result = false;
        $sg = null;

        if (!empty($_POST['InviteModel']['value']) || !empty($_GET['key'])) {
            $key = "";
	    if(!empty($_POST['InviteModel']['value'])){
		$key = $_POST['InviteModel']['value'];
            }
            if(!empty($_GET['key'])){
                $key = $_GET['key'];
            }

            $model = InviteModel::model()->findByCode($key);

            if (is_null($model)) {
                $model = new InviteModel();
                $model->addError('value', "Неверный код!");
            }
        } else {
            $model = new InviteModel();
            $model->addError('value', "Вы не ввели код!");
        }


        if (!$model->getIsNewRecord()) {
            $model->owner = Yii::app()->user->id;
            $model->active = 1;
            $model->active_date = new CDbExpression('CURRENT_TIMESTAMP');
            $model->save();

            if (!$model->hasErrors()) {
                $gameAccountsModel->saveAccount("finaldesire");
                $result = true;
            }
        }

/*        $this->render('index', array('model' => $model, 'user' => $user, 'result' => $result, 'sg' => $sg));*/

        $this->render("create", array("model" => $model, "user" => $user, "result" => $result, 'errors' => $errors));
    }

    /*
     * Смена пароля от учетной записи в игре
     */
    public function actionPassword()
    {

        //@sk old check on auth
        if (Yii::app()->user->isGuest) {
            $this->redirect('/user/login/');
            return;
        }
        $user = UserModel::model()->findByPk(Yii::app()->user->id);
        if (is_null($user)) {
            $this->redirect('/user/logout');
            return;
        }

        //@sk need check isset solgame account
        $gameAccountsModel = new GameAccountsModel();
        $haveAccount = $gameAccountsModel->checkAccountByName("solgame", Yii::app()->user->id);

        if(!$haveAccount){
            $this->redirect('/solgame/');
            return;
        }
    }
}
