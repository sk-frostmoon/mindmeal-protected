<?php

class SolgameController extends Controller
{
    private $_user;

    protected function beforeAction()
    {
        //@sk old check on auth
        if (Yii::app()->user->getIsGuest()) {
            $this->redirect('/user/login/');
        }
        $this->_user = UserModel::model()->findByPk(Yii::app()->user->id);
        if (is_null($this->_user)) {
            $this->redirect('/user/logout');
        }

        return true;
    }

    public function getUser()
    {
        return $this->_user;
    }

    /*
     * Состояние учетной записи (есть или нет)
     */
    public function actionIndex()
    {
        //@sk need check isset solgame account
        $gameAccountsModel = new GameAccountsModel();
        $haveAccount = $gameAccountsModel->checkAccountByName("solgame", $this->user->id);

        $this->render("index", array("user" => $this->user, "haveAccount" => $haveAccount));
    }

    /*
     * Создаем учетную запись
     */
    public function actionCreate()
    {
        try {
            $gameAccountsModel = new GameAccountsModel();
            $result = $gameAccountsModel->createAccount("solgame", $this->user->id);
            $errors = $gameAccountsModel->getErrors();
            if ($result === true) {
                $this->redirect("/solgame/");
            }
        } catch (Exception $e) {
            Yii::log($e->getMessage(), 'error');
        }

        $this->render("create", array("user" => $this->user, "result" => $result, 'errors' => $errors));
    }

    /*
     * Смена пароля от учетной записи в игре
     */
    public function actionPassword()
    {
        //@sk need check isset solgame account
        $gameAccountsModel = new GameAccountsModel();
        $haveAccount = $gameAccountsModel->checkAccountByName("solgame", $this->user->id);

        if (!$haveAccount) {
            $this->redirect('/solgame/');
        }

        throw new CHttpException(404, 'Аккаунт не найден');
    }
}
