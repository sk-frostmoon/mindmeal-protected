<?php

class SiteController extends Controller
{

    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

        $this->redirect($this->createUrl('/user/index'));
    }

    public function actionLogin()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

	if (Yii::app()->user->getIsGuest()) {
		$this->redirect($this->createUrl('/user/login'));
	} else {
		$this->redirect($this->createUrl('/user/profile'));
	}
    }
}
