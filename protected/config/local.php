<?php

return CMap::mergeArray(include('main.php'),
    array(
        'components'=>array(
            'mailer' => array(
                'renderOptions' => array(
                    'titles' => array(
                        'activate'           => Yii::t('mailer', 'Registration confirm'),
                        'forgotten_password' => Yii::t('mailer', 'Restore password'),
                        'password_restored'  => Yii::t('mailer', 'Password restored'),
                    ),
                ),
            ),
            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    array(
                        'class' => 'CFileLogRoute',
                        'enabled' => true, //YII_DEBUG_SHOW_PROFILER,
                        //'categories' => 'system.db.*',
                        'levels' => 'trace,info,error,warning',
                        //'ignoreAjaxInFireBug' => false,
                        //'filter' => array(
                        //    'class' => 'CLogFilter',
                        //    'prefixSession' => true,
                        //    'prefixUser'    => true,
                        //    'logUser'       => true,
                            //'logVars'       => true,
                        //),
                    ),
                    array(
                        'class' => 'CWebLogRoute',
                        'enabled' => false, //YII_DEBUG_SHOW_PROFILER,
                        'categories' => 'system.web.CHttpSession',
                    ),
                ),
            ),
        ),
        'params'=> array(
            'hostSalts' => array(
                'gk.sol_main.devs.mindmeal.ru'  => '107cad536d4bcccbd335bbf33762a8738177dc86',
                'ak.sol_main.devs.mindmeal.ru'  => '107cad536d4bcccbd335bbf33762a8738177dc86',
                'akr.sol_main.devs.mindmeal.ru' => '107cad536d4bcccbd335bbf33762a8738177dc86',
                'sk.sol_main.devs.mindmeal.ru'  => '107cad536d4bcccbd335bbf33762a8738177dc86',
            ),
        ),
    )
);

