<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
YiiBase::setPathOfAlias('rest', dirname(__FILE__).DIRECTORY_SEPARATOR.'../extensions/yii-rest-api/library/rest');
defined('IS_REST_REQUEST') or define('IS_REST_REQUEST', strpos($_SERVER['REQUEST_URI'], '/api/') === 0);

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath'=>realpath(__DIR__.DIRECTORY_SEPARATOR.'..'),
    'name'=>'Login Mindmeal',
    'theme'=>'mm_secure',

    'sourceLanguage'=>'en_us',
    'language'=>'ru',

    // preloading 'log' component
    'preload'=>array('log', 'mailer', 'events', 'restService'),

    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),

    'modules'=>array(
        // uncomment the following to enable the Gii tool
        /*
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'Enter Your Password Here',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
        ),
        */
    ),

    // application components
    'components'=>array(
        'events' => array(
            'class' => 'ext.events.Events',
        ),
        'statApi' => array(
            'class' => 'ext.stat_api.src.Yii.StatApiExtension',
            'statUrl' => 'https://stat.mindmeal.ru/stat_src/event.php',
        ),
        'restService' => array(
            'class' => '\rest\Service',
            'enable' => IS_REST_REQUEST,
            'authAdapterConfig' => array(
                'class' => 'RestAuthBasic',
            ),
        ),
        'messages' => array(
            'class' => 'CPhpMessageSource',
            'extensionPaths' => array(
                'mailer' => 'ext.mailer.messages',
            ),
        ),
        'session' => array(
            'cookieParams' => array(
                'lifetime' => 31536000, // 365 days
            ),
        ),
        /*
        'tokenCache' => array(
            'class' => 'CMemCached',
            ''
        ),*/
        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
            'class' => (IS_REST_REQUEST) ? 'RestWebUser' : 'WebUser',
            'loginUrl' => '/user/login',

            'onUserLogin'    => array('ProjectEvents', 'onUserLogin'),
            'onUserRegister' => array('ProjectEvents', 'onUserRegister'),
        ),
        'mailer' => array(
            'class'=>'application.extensions.mailer.AMQPExtension',
            'host' => '54.72.22.222',
            'port' => 5672,
            'username' => 'mm_user',
            'password' => 'auoYlPefy1',
            'virtualHost' => '/',

            'exchange'  => 'mailer',
            'queue'     => 'emails_all',

            'deliveryMode' => 2,
            'debug' => false,
            'projectClass' => 'SecureProject',
            'renderOptions' => array(
                'layout'   => 'application.views.layouts.mailer',
                'viewPath' => 'application.views.mailer',
                'titles' => array(),
                /*
                 * Make array with key as view name in viewPath in local.php config
                 * 'titles' => array(
                 *      'activate'           => Yii::t('mailer', 'Registration confirm'),
                 *      'forgotten_password' => Yii::t('mailer', 'Restore password'),
                 *      'password_restored'  => Yii::t('mailer', 'Password restored'),
                 * ),
                 */
            ),
        ),
        // uncomment the following to enable URLs in path-format
        //*
        'urlManager'=>array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'baseUrl' => '',
            'rules'=>array(
                'login' => 'user/login',
                'logout' => 'user/logout',
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                array('rest/<action>', 'pattern' => 'api/user/<action:\w+>', /*'verb'=>'POST', */'parsingOnly'=>true),
            ),
        ),
        //*/
        // uncomment the following to use a MySQL database
        'db'=> array(
            'connectionString' => 'mysql:host=localhost;dbname=mm_secure',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'jGLXUrI7Ma',
            'charset' => 'utf8',
        ),
        'sol_game_db' => array(
            'class' => 'CDbConnection',
            'connectionString' => 'mysql:host=176.56.184.159;dbname=core_cert',
            'emulatePrepare' => true,
            'username' => 'solweb',
            'password' => 'zeeh9EaNg7',
            'charset' => 'utf8',
        ),
        'tokenCache' => array(
            'class' => 'CMemCache',
            'useMemcached' => true,
            'hashKey' => false, // DO NOT HASH IT! Used in dashboards
            'keyPrefix' => 'token:',
            //'serializer' => true,
            'servers' => array(
                array('host' => '127.0.0.1', 'port' => 11211, 'weight' => 100),
            ),
        ),
        /*
        'secondDb'=>array(
            'class' => 'CDbConnection',
            'connectionString' => 'mysql:host=localhost;dbname=',
        ),
        */
        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
        'clientScript' => array(
                'packages' => array(
                    'jquery' => array(
                        'baseUrl' => '/js/',
                        'js' => array('jquery-1.11.0.min.js'),
                    ),
                    'jquery-ui' => array(
                        'baseUrl' => '/js/',
                        'js' => array('jquery-1.11.0.min.js'),
                        'depends' => array('jquery'),
                    ),
                    'bootstrap' => array(
                        'baseUrl' => '/js/',
                        'js' => array(
                            YII_DEBUG ? 'bootstrap.js' : 'bootstrap.min.js',
                        ),
                        'depends' => array('jquery'),
                    ),
                    'calendar' => array(
                        'baseUrl' => '/js/calendar/',
                        'js' => array('cal.js'),
                        'css' => array('cal.css'),
                        'depends' => array('bootstrap'),
                    ),
                    'jcrop' => array(
                        'baseUrl' => '/js/jcrop/',
                        'js' => array('jquery.Jcrop.min.js'),
                        'css' => array('jquery.Jcrop.min.css'),
                        'depends' => array('jquery'),
                    ),
                ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'sk@mindmeal.ru',
        'hostSalts' => array(
            // sha1(sha1(rand() . 'sol_local', true))
            'mindmeal.ru'    => '649e3a85d55b0c3a91e4529f646e5b0886ea4926',
            'solgame.ru'     => '51d33a3eb73063620fb3a825f2c43b85fece5d35',
            'playfd.ru'      => 'fb7a96e41686d1c79c23cea2faf8df811517a906',
            'finaldesire.ru' => 'fb7a96e41686d1c79c23cea2faf8df811517a906',
        ),
    ),
);
