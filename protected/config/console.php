<?php

YiiBase::setPathOfAlias('rest', dirname(__FILE__) . '/../extensions/yii-rest-api/library/rest');
defined('IS_REST_REQUEST') or define('IS_REST_REQUEST', true);

if (file_exists('local.php')) {
    $config = 'local.php';
} else {
    $config = 'main.php';
}
$config = require('local.php');

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath'=>__DIR__.DIRECTORY_SEPARATOR.'..',
    'name'=>'My Console Application',

    'language' => 'ru',
    'sourceLanguage' => 'en',

    // preloading 'log' component
    'preload'=>array('log', 'events', 'mailer'),

    'import'=> array(
        'application.models.*',
        'application.components.*',
        'application.migrations.repeatedly.*',
    ),

    'commandMap' => array(
        'migrate' => array(
            'class' => 'system.cli.commands.MigrateCommand',
            'connectionID' => 'migrate_db',
        ),
    ),

    // application components
    'components'=>array(
        'restService' => array(
            'class' => '\rest\Service',
            'enable' => IS_REST_REQUEST,
            'authAdapterConfig' => array(
                'class' => 'RestAuthBasic',
            ),
        ),
        'messages' => array(
            'class' => 'CPhpMessageSource',
            'extensionPaths' => array(
                'mailer' => 'ext.mailer.messages',
            ),
        ),

        'db'          => $config['components']['db'],
        'sol_game_db' => $config['components']['sol_game_db'],
        'migrate_db'=>array(
            'class' => 'CDbConnection',
            'connectionString' => 'sqlite:'.__DIR__.'/../data/migrations.db',
        ),

        'mailer'      => $config['components']['mailer'],

        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
            ),
        ),
    ),
);
