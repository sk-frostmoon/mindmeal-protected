<?php

class GameAccountsModel extends \CActiveRecord
{

//    public $user_d;
//    public $game;
//    public $created;


    public $games = array(
        'solgame'       => 1,
        'finaldesire'   => 2
    );

    /**
     * model
     *
     * @param string $className
     * @static
     * @access public
     * @return TagsModel
     */
    static public function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    /**
     * attributeNames
     *
     * @access public
     * @return array
     */
    public function attributeNames()
    {
        return array(
            'user_id',
            'game',
            //'created'
        );
    }

    /**
     * tableName
     *
     * @access public
     * @return string
     */
    public function tableName()
    {
        return 'game_accounts';
    }


    /**
     * rules
     *
     * @access public
     * @return array
     */
    public function rules()
    {
        return array(
            array('user_id, game', 'required'),
            array('user_id', 'numerical', 'min'=>1),
            array('game', 'in', 'range' => array_values($this->games)),
            array('created', 'default', 'value' => new CDbExpression('CURRENT_TIMESTAMP'), 'on' => 'insert'),
            array('user_id, game, created', 'default', 'on'=>'safe'),
        );
    }


    /*
     * check account by name (e.g. "finaldesire")
     * and by user_id
     */
    public function checkAccountByName($name, $user_id) {
        $user_id = (int)$user_id;

        if (!isset($this->games[$name])) {
            return false;
        }

        return $this->checkAccountByCode($this->games[$name], $user_id);
    }

    /*
     * check account by project id
     * and by user_id
     */
    public function checkAccountByCode($code, $user_id) {
        $user_id = (int)$user_id;
        $code    = (int)$code;

        if(!in_array($code, $this->games) || $user_id <= 0){
            return false;
        }

        $model  = self::model()->findByAttributes(array('game' => $code, 'user_id' => $user_id));
        return !is_null($model);
    }


    // public createAccount(game,userId) {{{ 
    /**
     * createAccount
     * 
     * @param mixed $game 
     * @param mixed $userId 
     * @access public
     * @return void
     */
    public function createAccount($game, $userId)
    {
        if ($this->checkAccountByName($game, $userId)) {
            $this->addError('game', 'Аккаунт уже существует');
            return false;
        }

        $result = false;
        switch ($game) {
            case "solgame":
                $model = new self('insert');
                $model->onCreateAccount = array('ProjectEvents', 'createSolAccount');
                $result = $model->saveAccount($game, $userId);

                Yii::trace(__METHOD__ . ' : ' . serialize($model));
                $this->addErrors($model->getErrors());
                break;
            case "finaldesire":
                $result = true;
                break;
        }
        return $result;
    }
    // }}}

    // public saveAccount(game,userId=null) {{{ 
    /**
     * saveAccount
     * 
     * @param mixed $game 
     * @param mixed $userId 
     * @access public
     * @return void
     */
    public function saveAccount($game, $userId = null) {
        $userId = isset($userId) ? $userId : Yii::app()->user->id; 
        $user = UserModel::model()->findByPk($userId);
        if (is_null($user)) {
            $this->addError('user_id', 'Некорректный ID пользователя');
            return false;
        }

        $this->scenario = 'insert';
        $this->attributes = array(
            'user_id' => $user->id,
            'game'    => $this->games[$game],
        );
        if ($this->save()) {
            if ($this->hasEventHandler('onCreateAccount')) {
                $this->onCreateAccount(new CEvent($this, array('user' => $user)));
            }
        }

        if ($this->hasErrors()) {
            Yii::log(__METHOD__ . ' : ' . serialize($this->getErrors()), 'error');
        }

        return !$this->hasErrors();
    }
    // }}}

    public function onCreateAccount($event)
    {
        $this->raiseEvent('onCreateAccount', $event);
    }


    /**
     * afterFind
     * 
     * @access public
     * @return void
     */
    public function afterFind() {
        parent::afterFind();
        $this->created = strtotime($this->created . " +0400");
    }

    /**
     * beforeSave
     * 
     * @access public
     * @return bool
     */
    public function beforeSave() {
        return parent::beforeSave();
    }

    public function afterSave() {
        parent::afterSave();
    }
}
