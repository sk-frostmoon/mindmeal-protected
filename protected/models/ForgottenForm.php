<?php

class ForgottenForm extends CFormModel
{
    public $email;

    private $_user;
    
    public function rules()
    {
        return array(
            array('email', 'required'),
            array('email', 'email'),
            array('email', 'validEmailUser'),
        );
    }

    public function validEmailUser($attribute, $params)
    {
        $this->_user = UserModel::model()->findByAttributes(array('email' => $this->email));

        if (is_null($this->_user))
        {
            $this->addError($attribute, 'User with this E-Mail not founded');
        }
    }

    public function attributeLabels()
    {
        return array(
            'email' => Yii::t('secure', 'E-Mail'),
        );
    }

    public function mail()
    {
        if ($this->_user instanceof UserModel) {
            $url = CHtml::encode( Yii::app()->createAbsoluteUrl('/user/restore', array('token' => $this->_user->getToken())) ); 
            Yii::app()->mailer->postRenderPublish($this->_user->email, SecureProject::TPL_FORGOTTEN_PASSWORD, array(
                'restoreUrl' => $url,
                'restoreUrlLabel' => $url,
            ));
            //Yii::app()->mailer->renderPublish($this->_user->email, false, 'forgotten_password', array(
            //    'user' => $this->_user,
            //));
            return true;
        } else {
            return false;
        }
    }
}
