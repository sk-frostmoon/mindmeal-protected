<?php

class RestUserModel extends UserModel
{
    protected $_primaryKey = 'id';

    // Only for rest-registration
    public $crc;

    static public function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getAttributeNames()
    {
        $attr = parent::getAttributeNames();
        $attr[] = 'host';
        $attr[] = 'crc';
        return $attr;
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = array('host, crc', 'required', 'on' => 'register');
        $rules[] = array('host', 'checkHost', 'on'=>'register');
        $rules[] = array('host, src', 'default', 'on' => 'safe');

        return $rules;
    }

    // public checkHost(attribute,params) {{{ 
    /**
     * checkHost
     * 
     * @param string $attribute 
     * @param array $params 
     * @access public
     * @return void
     */
    public function checkHost($attribute, $params)
    {
        $salts = isset(Yii::app()->params['hostSalts']) ? Yii::app()->params['hostSalts'] : array();

        $host = preg_replace('/^(http:\/\/|)(www\.|)([^\/]+)(|\/.*)$/', '\3', $this->$attribute);

        $message = Yii::t('secure', 'There are technical problems. Please try to register your account <a target="_blank" href="http://login.mindmeal.ru/user/register?url={host}">here</a>');
        $message = strtr($message, array('{host}' => CHtml::encode($this->$attribute)));

        if ((!$host) || (!isset($salts[$host]))) {
            $this->addError($attribute . '_code', 404);
            $this->addError($attribute, $message);
            return;
        }

        $args  = array(
            'RestUserModel' => array(
                'username'  => $this->username,
                'email'     => $this->email,
                'password'  => $this->password,
                'passwordConfirm'   => $this->passwordConfirm,
                'licenseAgree'      => $this->licenseAgree,
                'host'      => $this->host,
            ),
        );

        $queryCheck = sha1(http_build_query($args) . $salts[$host]);
        if (strcmp($queryCheck, $this->crc) !== 0) {
            $this->addError($attribute . '_code', 400);
            $this->addError($attribute, $message);
            return;
        }
    }
    // }}}
}
