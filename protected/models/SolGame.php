<?php

class SolGame extends CActiveRecord
{
    /**
     * model
     *
     * @param string $className
     * @static
     * @access public
     * @return TagsModel
     */
    static public function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * tableName
     *
     * @access public
     * @return string
     */
    public function tableName()
    {
        return 'SecurityInfo';
    }

    /**
     * @return CDbConnection|null|string
     */
    public function getDbConnection() {
            return Yii::app()->sol_game_db;
    }


    /**
     * rules
     *
     * @access public
     * @return array
     */
    public function rules()
    {
        return array(
            array('s_id, security_name, security_code, event_win', 'required'),
            array('s_id, security_name, security_code, event_win', 'default', 'on'=>'safe'),
        );
    }

    public function createAccount($userId = NULL)
    {
        $oldScenario = UserModel::model()->scenario;

        UserModel::model()->scenario = 'sol_register';
        $user = UserModel::model()->findByPk(is_null($userId) ? Yii::app()->user->id : $userId);
        UserModel::model()->scenario = $oldScenario;

        if (is_null($user)) {
            $this->addError("sol_register", 'Невозможно получить пользователя. Попробуйте еще раз.');
            return false;
        }

        $check = self::model()->findByAttributes(array('s_id' => $user->id));
        if (!is_null($check)) {

            $this->addError("sol_account_exists", 'Учетная запись для SolGame уже была создана. Если вы ее не создавали обратитесь в службу поддержки.');
            return false;
        }

        $this->attributes = array(
            "s_id" => $user->id,
            "security_name" => $user->username,
            "security_code" => new CDbExpression("CONCAT('*', UPPER(SHA1(UNHEX('". $user->password ."'))))"),
            "event_win" => 1
        );
        $result = $this->save();
        /*
        $query = $this->getDbConnection()->createCommand();
        $result = $query->insert($this->tableName(), array(
            "s_id" => $user->id,
            "security_name" => $user->username,
            "security_code" => new CDbExpression("CONCAT('*', UPPER(SHA1(UNHEX('". $user->password ."'))))"),
            "event_win" => 1
        ));
        */

        if ($result == false) {
            $this->addError("sol_register", 'Не удалось создать учетную запись для SolGame. Обратитесь в службу поддержки.');
            return false;
        }

        if ( ( ! $this->hasErrors() ) && $this->hasEventHandler('onAccountCreated') ) {
            $this->onAccountCreated(new CEvent($this, array('project' => 'sol', 'user' => $user)));
        }

        return true;

        //INSERT INTO core_cert.SecurityInfo (s_id, security_name, security_code, event_win) VALUES (INDEX, "USER_ID", password("USER_PASSWORD"), 1);
    }

    public function changePassword($userId = null) {
        $oldScenario = UserModel::model()->scenario;

        UserModel::model()->scenario = 'sol_change_password';
        $user = UserModel::model()->findByPk(is_null($userId) ? Yii::app()->user->id : $userId);
        UserModel::model()->scenario = $oldScenario;


        //@sk
        try {
            if (is_null($user)) {
                //$this->addError("sol_change_password", 'Невозможно получить пользователя. Попробуйте еще раз.');
                return false;
            }

            $check = self::model()->findByAttributes(array('s_id' => $user->id));
            if (is_null($check)) {
                //$this->addError("sol_change_password", 'Учетная запись для SolGame е была создана. Если вы ее не создавали обратитесь в службу поддержки.');
                return false;
            }
            $check->security_code = new CDbExpression("CONCAT('*', UPPER(SHA1(UNHEX('". $user->password ."'))))");
            $check->save();

        } catch(CDbException $e) {
            Yii::log($e->getMessage(), 'sol_game_db');
            return false;
        }

        return true;
    }

    public function onAccountCreated($event)
    {
        Yii::trace(__METHOD__ . ' =>  onAccountCreated');
        $this->raiseEvent('onAccountCreated', $event);
    }
}
