<?php

class InviteModel extends CActiveRecord
{
    protected $_primaryKey = 'id';

    protected $_token;
    public  $licenseAgree;
    public  $passwordConfirm;
    public  $passwordOld;
    private $_currentPasswordOld;

//    public $id;
//    public $value;
//    public $project;
//    public $active;
//    public $active_date;
//    public $owner;



    /**
     * model
     *
     * @param string $className
     * @static
     * @access public
     * @return TagsModel
     */
    static public function model($className = __CLASS__)
    {
        return parent::model($className);
    }




    /**
     * attributeNames
     *
     * @access public
     * @return array
     */
    public function attributeNames()
    {
        return array(
            'id',
            'value',
            'project',
            'active',
            'active_date',
            'owner'
        );
    }



    public function attributeLabels()
    {
        static $labels;
        if (!isset($labels)) {
            return array(
                'id'            => Yii::t('invite', 'ID'),
                'value'         => Yii::t('invite', 'Invite Key'),
                'project'       => Yii::t('invite', '####'),
                'active'        => Yii::t('invite', '####'),
                'active_date'   => Yii::t('invite', 'Activated from'),
                'owner'         => Yii::t('invite', 'Owner'),
                //'licenseAgree'  => sprintf(Yii::t('secure', 'I agree <a class="license-agreement" data-toggle="modal" data-target="#license_modal" href="#">a license</a>'), Yii::app()->createUrl('user/license')),
            );
        }

        return $labels;
    }


    /**
     * tableName
     *
     * @access public
     * @return string
     */
    public function tableName()
    {
        return 'invite_key';
    }


    /**
     * rules
     *
     * @access public
     * @return array
     */
    public function rules()
    {
        return array(
            array('value, owner', 'required'),
            array('value', 'checkInvite'),
            array('owner', 'checkOwner'),
            array('id, value, project, active, active_date, owner', 'default', 'on'=>'safe'),
        );
    }


    public function checkInvite($attribute, $params){
        $code  = self::model()->find('value=:value', array(':value' => $this->{$attribute}));
        if(!$code || $code->owner != 0){
            $this->addError($attribute, "Неверный Инвайт-Код!");
        }
    }

    public function checkOwner($attribute, $params){
        $code  = self::model()->find('owner=:owner', array(':owner' => $this->{$attribute}));
        if(!is_null($code)){
            $this->addError('value', "Вы уже активировали Инвайт-Код");
        }
    }

    /**
     * afterFind
     * 
     * @access public
     * @return void
     */
    public function afterFind() {
        parent::afterFind();
        $this->active_date = strtotime($this->active_date . " +0400");
    }

    /**
     * beforeSave
     * 
     * @access public
     * @return bool
     */
    public function beforeSave() {
        return parent::beforeSave();
    }

    public function afterSave() {
        parent::afterSave();
    }


    /**
     * Find Key by Value
     * @param $key
     * @throws Exception
     * @return mixed
     */
    public function findByCode($key){
        if(empty($key)){
            throw new Exception('Empty Code Value in InviteModel::findByCode');
        }

        $result = self::model()->find('value=:value', array(':value' => $key));

        return $result;
    }


}
