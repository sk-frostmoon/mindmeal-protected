<?php

class RestfullToken extends CFormModel // extends CActiveRecord
{
    const STATE_OFFLINE  = 'offline';
    const STATE_LAUNCHER = 'launcher';
    const STATE_ONLINE   = 'online';

    const STATUS_IDLE           = 'idle';
    const STATUS_LOBBY          = 'lobby';
    const STATUS_BROKEN         = 'broken_session';
    const STATUS_WAITING_QUEUE  = 'waiting_queue';
    const STATUS_IN_GAME        = 'in_game';

    private $_detectPingers = array(
            'lastActiveTime' => 0,
            'bothPingers' => false,
    );
    /**
     * token 
     * 
     * @var string
     * @access public
     */
    public $token;
    /**
     * application 
     * 
     * @var string 
     * @access protected
     */
    protected $application;
    protected $clientUniqueId;
    /**
     * joinedTime 
     * Unit timestamp of last login
     * 
     * @var int
     * @access public
     */
    public $joinedTime;

    /**
     * state
     * State of the user
     * 
     * @var string RestfullToken::STATE_*
     * @access public
     */
    public $state = self::STATE_OFFLINE;
    /**
     * status
     * Status of user then him online
     * 
     * @var string RestfullToken::STATUS_*
     * @access public
     */
    public $status = self::STATUS_IDLE;

    /**
     * user 
     * 
     * @var UserModel
     * @access public
     */
    public $user = null;

    /**
     * level
     * User level
     * 
     * @var int
     * @access public
     */
    public $level = 0;
    /**
     * achivements 
     * Array of achivements IDs
     * 
     * @var array
     * @access public
     */
    public $achivements = array();

    // public findByEmail(userEmail) {{{ 
    /**
     * findByEmail
     * 
     * @param string $userEmail 
     * @static
     * @access public
     * @return false|RestfullToken
     */
    static public function findByEmail($userEmail)
    {
        return Yii::app()->tokenCache->get($userEmail);
    }
    // }}}

    // public getAllKeys() {{{ 
    /**
     * getAllKeys
     * Get all tokens keys without prefix of tokenCache
     * 
     * @static
     * @access public
     * @return array
     */
    static public function getAllKeys()
    {
        $tc = Yii::app()->tokenCache;
        $keys = array();
        foreach ($tc->getMemCache()->getAllKeys() as $key) {
            if (strpos($key, $tc->keyPrefix) === 0) {
                $keys[] = substr($key, strlen($tc->keyPrefix));
            }
        }

        return $keys;
    }
    // }}}

    // public rules() {{{ 
    /**
     * rules
     * 
     * @access public
     * @return void
     * /
    public function rules()
    {
        return array(
//                array('email', 'required'),
//                array('email', 'email'),
        );
    }
    // }}} //*/

    static public function getApplications()
    {
        return array(
            'finaldesire' => 'Final Desire',
        );
    }
    // public getStatesConfig() {{{
    /**
     * getStatesConfig
     * 
     * @static
     * @access public
     * @return array
     */
    static public function getStatesConfig()
    {
        return array(
                self::STATE_OFFLINE  => array(
                    'defaultStatus' => self::STATUS_IDLE,
                    'statuses' => array(
                        self::STATUS_IDLE => true,
                    ),
                ),
                self::STATE_LAUNCHER => array(
                    'defaultStatus' => self::STATUS_IDLE,
                    'statuses' => array(
                        self::STATUS_IDLE => true,
                    ),
                ),
                self::STATE_ONLINE   => array(
                    'defaultStatus' => self::STATUS_LOBBY,
                    'statuses' => array(
                        self::STATUS_LOBBY   => true,
                        self::STATUS_WAITING_QUEUE  => true,
                        self::STATUS_IN_GAME        => true,
                    ),
                ),
        );
    }
    // }}}

    // public getStatusesConfig() {{{ 
    /**
     * getStatusesConfig
     * 
     * @static
     * @access public
     * @return array
     */
    static public function getStatusesConfig()
    {
        return array(
                self::STATUS_IDLE           => array( ),
                self::STATUS_BROKEN         => array( ),
                self::STATUS_WAITING_QUEUE  => array( ),
                self::STATUS_IN_GAME        => array( ),
        );
    }
    // }}}

    // public setApplication(app) {{{ 
    /**
     * setApplication
     * 
     * @param string $app 
     * @access public
     * @return boolean
     */
    public function setApplication($app)
    {
        $apps = self::getApplications();
        // Only when not joined to any game
        if (isset($apps[$app]) && empty($this->application)) {
            if ($this->state == self::STATE_OFFLINE) {
                $this->application = $app;
                $this->state  = self::STATE_LAUNCHER;
                $this->status = self::STATUS_IDLE;
            }
            return true;
        }
        return false;
    }
    // }}}

    // public endApplication() {{{ 
    /**
     * endApplication
     * 
     * @access public
     * @return void
     */
    public function endApplication()
    {
        $this->application = null;
        $this->state       = self::STATE_OFFLINE;
        $this->status      = self::STATUS_IDLE;
    }
    // }}}

    // public getApplication() {{{ 
    /**
     * getApplication
     * 
     * @access public
     * @return string
     */
    public function getApplication()
    {
        return $this->application;
    }
    // }}}

    public function setUniqueId($id = 0)
    {
        $this->clientUniqueId = $id;
        return true;
    }

    // public getState() {{{ 
    /**
     * getState
     * 
     * @access public
     * @return void
     */
    public function getState()
    {
        return $this->state;
    }
    // }}}

    // public setState(value) {{{ 
    /**
     * setState
     * 
     * @param string $value RestfullToken::STATE_*
     * @access public
     * @return boolean
     */
    public function setState($value)
    {
        $stateConfig = self::getStatesConfig();
        if ($this->user && isset($stateConfig[$value])) {
            $this->state = $value;
            $this->status = $stateConfig[$value]['defaultStatus'];
            return true;
        }
        return false;
    }
    // }}}

    // public getStatus() {{{ 
    /**
     * getStatus
     * 
     * @access public
     * @return void
     */
    public function getStatus()
    {
        return $this->status;
    }
    // }}}

    // public setStatus(value) {{{ 
    /**
     * setStatus
     * 
     * @param mixed $value 
     * @access public
     * @return void
     */
    public function setStatus($value)
    {
        $stateConfig = self::getStatesConfig();
        if (isset($stateConfig[$this->state]['statuses'][$value])) {
            $this->status = $value;
            return true;
        }
        return false;
    }
    // }}}

    // public save(validate=true,attributes=false) {{{ 
    /**
     * save
     * 
     * @param boolean $validate 
     * @param array $attributes Array of model attributes 
     * @access public
     * @return boolean
     */
    public function save($validate = true, $attributes = false)
    {
        try {
            return \Yii::app()->tokenCache->set($this->user->email, $this, 60);
        } catch (\CException $e) {
            \Yii::log($e->getMessage(), 'error');
            return false;
        }
    }
    // }}}

    // public byebye() {{{ 
    /**
     * byebye
     * 
     * @access public
     * @return void
     */
    public function byebye()
    {
        \Yii::app()->tokenCache->delete($this->user->email);
    }
    // }}}

    // public touchAlive(duration=60) {{{ 
    /**
     * touchAlive
     * 
     * @param int $duration 
     * @access public
     * @return void
     */
    public function touchAlive($duration = 60)
    {
        if ($this->_detectPingers['lastActiveTime']+29 > time()) {
            $this->_detectPingers['bothPingers'] = true;
        } else {
            $this->_detectPingers['bothPingers'] = false;
        }
        $this->_detectPingers['lastActiveTime'] = time();
        $this->save();
        //$tc = \Yii::app()->tokenCache;
        //$tc->getMemCache()->touch($tc->keyPrefix.$this->user->email, $duration);
    }
    // }}}

    // public checkBothPingers() {{{ 
    /**
     * checkBothPingers
     * 
     * @access public
     * @return void
     */
    public function checkBothPingers()
    {
        return $this->_detectPingers['bothPingers'];
    }
    // }}}

    public function getDetectData()
    {
        return $this->_detectPingers;
    }
}
