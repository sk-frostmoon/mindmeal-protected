<?php

class UserModel extends CActiveRecord
{
    protected $_primaryKey = 'id';

    protected $_token;
    public  $licenseAgree;
    public  $passwordConfirm;
    public  $passwordOld;
    private $_currentPasswordOld;

    public $host;

//    public $id;
//    public $username;
//    public $password;
//    public $email;
//    public $bdate;
//    public $fname;
//    public $lname;

    static public $birthdayFormat = array(
            'user'   => 'dd/mm/yyyy',
            'check'  => 'd/m/Y',
            'regexp' => '/^(?P<day>[0-9]+)\/(?P<month>[0-9]+)\/(?P<year>[0-9]+)$/ui',
    );

    // public __unset(varname) {{{ 
    /**
     * __unset
     * 
     * @param mixed $varname 
     * @access public
     * @return void
     */
    public function __unset($varname)
    {
        if (isset($this->$varname) && $varname == 'password') {
            // @ak: When you unset($user->password) you are breaking the $this->_token generation if it's not setted early
            throw new Exception('Use UserModel::safeFlushPassword() instead unset(UserModel::password)');
        }

        parent::__unset($varname);
    }
    // }}}

    // public model(className=__CLASS__) {{{ 
    /**
     * model
     *
     * @param string $className
     * @static
     * @access public
     * @return TagsModel
     */
    static public function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    // }}}

    // public attributeNames() {{{ 
    /**
     * attributeNames
     *
     * @access public
     * @return array
     */
    public function attributeNames()
    {
        return array(
            'id',
            'username',
            'licenseConfirm',
            'password',
            'passwordOld',
            'passwordConfirm',
            'email',
            'bdate',
            'fname',
            'lname',
            'activated',
        );
    }
    // }}}

    public function attributeLabels()
    {
        static $labels;
        if (!isset($labels)) {
            return array(
                'id'            => Yii::t('secure', 'ID'),
                'username'      => Yii::t('secure', 'Login'),
                'password'      => Yii::t('secure', 'Password'),
                'passwordOld'   => Yii::t('secure', 'Current Password'),
                'passwordConfirm' => Yii::t('secure', 'Password Confirm'),
                'email'         => Yii::t('secure', 'Email'),
                'bdate'         => Yii::t('secure', 'Birthday'),
                'fname'         => Yii::t('secure', 'First name'),
                'lname'         => Yii::t('secure', 'Last name'),
                'activated'     => Yii::t('secure', 'User activated'),

                'restfullToken' => Yii::t('secure', 'Restfull Token'),
                'last_visit'    => Yii::t('secure', 'Last Visited'),
                'licenseAgree'  => sprintf(Yii::t('secure', 'I agree with <button type="button" class="license-agreement" data-toggle="modal" data-target="#license_modal">a license</button>'), Yii::app()->createUrl('user/license')),
            );

            /*
            $langs = Languages::model()->findAll();
            foreach($langs as $l) {
                $labels['title_' . $l->abbrev] = sprintf(Yii::t('languages', 'Заголовок для языка "%s"'), $l->title);
            }
            */
        }

        return $labels;
    }

    // public tableName() {{{ 
    /**
     * tableName
     *
     * @access public
     * @return string
     */
    public function tableName()
    {
        return 'user';
    }
    // }}}

    public function setScenario($value)
    {
        if (strcmp($value, 'changePassword') === 0) {
            if (is_null($this->password)) {
            } else {
                $this->_currentPasswordOld = $this->password;
            }
        }
        parent::setScenario($value);
    }

    // public rules() {{{
    /**
     * rules
     *
     * @access public
     * @return array
     */
    public function rules()
    {
        $rules = array(
                // Register scenario
                array('email, password, passwordConfirm, username, licenseAgree', 'required', 'on'=>'register'),
                array('licenseAgree', 'checkAgreement', 'on'=>'register'),
                array('email', 'email', 'on'=>'register'),
                array('email, username', 'unique', 'caseSensitive' => false, 'on'=>'register'),
                array('password', 'length', 'min'=>6, 'max'=>128, 'on'=>'register'),
                array('username', 'length', 'min'=>4, 'max'=>32, 'on'=>'register'),
                array('username', 'match',  'pattern' => '/^[A-Za-z0-9]+$/', 'on'=>'register', 'message' => Yii::t('secure', 'Username must contains only A-Z and 0-9 characters')),
                array('username', 'checkOnGmCharacter', 'on' => 'register', 'message' => Yii::t('secure', 'Username already exist')),
                array('passwordConfirm', 'compare', 'compareAttribute'=>'password', 'on'=>'register', 'message' => Yii::t('secure', 'Passwords must be repeated exactly')),
                array('bdate', 'validBirthday', 'on'=>'register'),
                array('fname, lname', 'length', 'min'=>0, 'max'=>'128', 'on'=>'register'),
                array('utm_source, utm_campaign, utm_medium', 'default', 'on'=>'register'),

                // Profile scenario
                array('bdate', 'validBirthday', 'on'=>'profile'),
                array('fname, lname, username', 'length', 'min'=>0, 'max'=>'128', 'on'=>'profile'),

                // Restore/changePassword scenario
                array('passwordOld, password, passwordConfirm', 'required', 'on'=>'changePassword'),
                array('password, passwordConfirm',              'required', 'on'=>'restore'),

                array('password', 'length', 'min'=>6, 'max'=>128, 'on'=>'restore, changePassword'),
                array('passwordOld',     'comparePasswordOld',  'compareAttribute'=>'_token', 'on'=>'changePassword', 'message' => Yii::t('secure', 'Current password is wrong')),
                array('passwordConfirm', 'compare',             'compareAttribute'=>'password', 'on'=>'restore, changePassword', 'message' => Yii::t('secure', 'Fields "{compareAttribute}" and "{attribute}" does not match')),

                // Activation scenario
                array('activated', 'numerical', 'allowEmpty'=> false, 'on'=>'activation'),

                array('id, username, password, email, bdate, fname, lname, created, updated, activated, utm_source, utm_campaing, utm_medium', 'default', 'on'=>'safe'),
        );

        return $rules;
    }
    // }}}

    public function checkAgreement($attribute, $options)
    {
        if ($this->$attribute != '1') {
            $this->addError($attribute, Yii::t('secure', 'You must confirm with ours license agreement to finish registration'));
        }
    }

    public function checkOnGmCharacter($attribute, $options) {
        if (strstr(strtolower($this->{$attribute}), "coregm") !== false) {
            $this->addError($attribute, Yii::t('secure', 'Username already exist'));
        }
    }

    // public comparePasswordOld(attribute,options) {{{ 
    /**
     * comparePasswordOld
     * 
     * @param string $attribute 
     * @param mixed $options 
     * @access public
     * @return void
     */
    public function comparePasswordOld($attribute, $options)
    {
        if (strcmp(sha1($this->email . ":" . sha1($this->{$attribute})), $this->{$options['compareAttribute']}) !== 0) {
            $message = isset($options['message']) ? $options['message'] : Yii::t('secure', 'Field {attribute} is wrong.');
            $params = array(
                    '{attribute}'=>$this->getAttributeLabel($attribute),
            );
            $this->addError($attribute, strtr($message,$params));
        }
    }
    // }}}

    // public validBirthday(attribute,params) {{{ 
    /**
     * validBirthday
     * 
     * @param string $attribute 
     * @param mixed $params 
     * @access public
     * @return void
     */
    public function validBirthday($attribute, $params)
    {
        if (!empty($this->bdate)) {

            $_date = DateTime::createFromFormat("d . m . Y", $this->bdate);
            if($_date){
                $this->bdate = $_date->format(self::$birthdayFormat['check']);
            }

            preg_match(self::$birthdayFormat['regexp'], $this->bdate, $matches);
            if (count($matches) == 7) { // There are keys: 0, 'day', 1, 'month', 2, 'year', 3
                $str = $matches['year'] . '-' . $matches['month'] . '-' . $matches['day'] . ' 00:00:00';

                if (strcmp($this->bdate, date(self::$birthdayFormat['check'], strtotime($str))) !== 0) {
                    $this->addError($attribute, 'Field {attribute} incorrect');
                }
            } else {
                $this->addError($attribute, 'Field {attribute} incorrect');
            }
        }
    }
    // }}}

    // public relations() {{{ 
    /**
     * relations
     * 
     * @access public
     * @return array
     */
    public function relations()
    {
        return array(
                //'token' => array(self::BELONGS_TO, 'RestfullToken', array('email'=>'username')), // @ak: No more relation. Only plain model used.
        );
    }
    // }}}

    // public getIsActivated() {{{ 
    /**
     * getIsActivated
     * 
     * @access public
     * @return void
     */
    public function getIsActivated()
    {
        return ($this->activated == 1);
    }
    // }}}

    // public findByUsername(name) {{{ 
    /**
     * findByUsername
     * 
     * @param mixed $name 
     * @access public
     * @return void
     */
    public function findByUsername($name) {
        $result = self::model()->find('username=:username', array(':username' => $name));
        return $result;
    }
    // }}}

    public function authHost() {
        $params = Yii::app()->session['need_site_auth'];
        if(count($params) >= 2 && !empty($params['host']) && !empty($params['token'])){
            $db = Yii::app()->db;
            $query = $db->createCommand("DELETE FROM sessions WHERE host = :host AND token = :token");
            $query->bindParam('host',  $params['host']);
            $query->bindParam('token', $params['token']);
            $messages = $query->execute();

            $query = $db->createCommand("INSERT INTO sessions (host, token, user) VALUES (:host, :token, :user)");
            $query->bindParam('host',  $params['host']);
            $query->bindParam('token', $params['token']);
            $query->bindParam('user', Yii::app()->user->getId());
            $messages = $query->execute();

            Yii::app()->session['need_site_auth'] = null;

        } else {
            return false;
        }
    }

    public function logoutHosts(){
        $userId = Yii::app()->user->getEmail();

        $db = Yii::app()->db;
        $query = $db->createCommand("DELETE FROM sessions WHERE user = :user");
        $query->bindParam('user',  $userId);
        $messages = $query->execute();

    }

    // public getAvatarUrl(size=false) {{{ 
    /**
     * getAvatarUrl
     * Return url to avatar
     * 
     * @param string $size Format to resize can be '100x100', '-x100', '100x-'
     * @access public
     * @return string
     */
    public function getAvatarUrl($size=false)
    {
        $filename = $this->id . '.png';
        if (!file_exists(Yii::getPathOfAlias('webroot') . '/images/avatars/' . $filename)) {
            $filename = 'default.png';
        }
        $url = Yii::app()->createAbsoluteUrl('/images/avatars/') . '/' . $filename;
        $params = array();
        if (is_string($size)) {
            if (preg_match('/^([0-9]+|\-)x([0-9]+|\-)$/', $size)) {
                $params[] = 'size=' . htmlentities($size);
            }
        }

        if (!empty($params)) {
            $url .= '?' . implode('&', $params);
        }
        return $url;
    }
    // }}}

    // public getRestfullToken() {{{ 
    /**
     * getRestfullToken
     * 
     * @access public
     * @return void
     */
    public function getRestfullToken()
    {
        if ($this->getIsNewRecord()) {
            return NULL;
        }

        return RestfullToken::findbyEmail($this->email);
    }
    // }}}

    // public getToken() {{{ 
    /**
     * getToken
     * 
     * @access public
     * @return string
     */
    public function getToken($hard = false)
    {
        if ((!$this->getIsNewRecord()) && (!isset($this->_token)) && (isset($this->password))) {
            $this->_token = sha1($this->email . ':' . $this->password);
        }
        return $this->_token;
    }
    // }}}

    // public safeFlushPassword() {{{ 
    /**
     * safeFlushPassword
     * 
     * @access public
     * @return void
     */
    public function safeFlushPassword()
    {
        if (!$this->getIsNewRecord()) {
            $this->getToken(); // @ak: Save current token to self::$_token
        }
        
        if(self::model()->scenario != 'sol_register' && self::model()->scenario != 'sol_change_password'){
                parent::__unset('password');
        }
    }
    // }}}

    // public findByToken(token) {{{ 
    /**
     * findByToken
     * 
     * @param mixed $token 
     * @access public
     * @return void
     */
    public function findByToken($token)
    {
        return $this->find('SHA1(concat(email, \':\', password)) = :token', array(':token'=>$token));
    }
    // }}}

    // public afterFind() {{{ 
    /**
     * afterFind
     * 
     * @access public
     * @return void
     */
    public function afterFind() {
        parent::afterFind();
        $this->created = strtotime($this->created . " +0400");
        $this->updated = strtotime($this->updated . " +0400");
        $this->bdate   = (int)$this->bdate;

        // To stop showing password hash to user
        //unset($this->_token);
        $this->safeFlushPassword(); // @ak: will keep a current token
    }
    // }}}

    // public beforeSave() {{{ 
    /**
     * beforeSave
     * 
     * @access public
     * @return bool
     */
    public function beforeSave() {
        if (!empty($this->bdate) && is_string($this->bdate)) {
            preg_match(self::$birthdayFormat['regexp'], $this->bdate, $matches);
            // There are keys: 0, 'day', 1, 'month', 2, 'year', 3
            $str = $matches['year'] . '-' . $matches['month'] . '-' . $matches['day'] . ' 00:00:00';
            $this->bdate = strtotime($str);
        }

        if (is_string($this->password)) {
            $this->password = SHA1($this->password);
            // @ak Token is a auto-generated
        }

        if ($this->getIsNewRecord()) {
            $this->created = new CDbExpression('CURRENT_TIMESTAMP');
        } else {
            unset($this->created);
        }
        $this->updated = new CDbExpression('CURRENT_TIMESTAMP');

        return parent::beforeSave();
    }
    // }}}

    public function afterSave() {
        parent::afterSave();
        //unset($this->_token);
        $this->safeFlushPassword();
    }
}
