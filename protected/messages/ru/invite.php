<?php

return array(
        'ID' => 'ID',
        'Invite Key' => 'Ключ приглашения',
        'Activated from' => 'Активирован',
        'Owner' => 'Владелец',
        'Enter your invite key' => 'Введите инвайт-код',

        'All products' => 'Все проекты',
);
