<?php

return array(
        'ID' => 'ID',
        'Login' => 'Имя пользователя',
        'Password' => 'Пароль',
        'Current Password' => 'Текущий пароль',
        'Password Confirm' => 'Повторите пароль',
        'Email' => 'E-Mail',
        'Birthday' => 'День рождения',
        'First name' => 'Имя',
        'Last name' => 'Фамилия',
        'User activated' => 'Пользователь активрован',
        'Restfull Token' => 'Restfull-ключ',
        'Last Visited' => 'Последний визит',

        // licenseAgree
        'I agree <a class="license-agreement" data-toggle="modal" data-target="#license_modal" href="#">a license</a>' => 'Я согласен с условиями <a class="license-agreement" data-toggle="modal" data-target="#license_modal" href="#">лицензионного соглашения</a>',
        'I agree with <button type="button" class="license-agreement" data-toggle="modal" data-target="#license_modal">a license</button>' => 'Я согласен с условиями <button type="button" class="license-agreement" data-toggle="modal" data-target="#license_modal">лицензионного соглашения</button>',

        // Titles
        'Restore password' => 'Восстановить пароль',
        'Changing password' => 'Изменение пароля',

        // Model Messages
        'User with this E-Mail not founded' => 'Пользователь с таким E-Mail не найден',
        'Current password is wrong' => 'Текущий пароль указан неверно',
        'Field {attribute} is wrong.' => 'Поле "{attribute}" указано неверно.',
        'Fields "{compareAttribute}" and "{attribute}" does not match' => 'Поля "{compareAttribute}" и "{attribute}" не совпадают',
        'Passwords must be repeated exactly' => 'Пароли должны полностью совпадать',
        'Username must contains only A-Z and 0-9 characters' => 'Имя пользователя может содержать только символы A-Z и цифры',
        'Username already exist' => 'Такое имя пользователя уже используется',
        'There are technical problems. Please try to register your account <a target="_blank" href="http://login.mindmeal.ru/user/register?url={host}">here</a>' => 'Возникли технические проблемы. Попробуйте зарегистрировать свой аккаунт <a target="_blank" href="http://login.mindmeal.ru/user/register?url={host}">здесь</a>',
        'You must confirm with ours license agreement to finish registration' => 'Для завершения регистрации Вам необходимо подтвердить свое согласие с условиями лицензионного соглашения',

        'Notifications' => 'Рассылка',
        'Notify settings' => 'Настройка уведомлений',
        'Additional configuration' => 'Расширенная конфигурация',
        'All products' => 'Все проекты',
        'Dear User' => 'Уважаемый пользователь',

        'Back' => 'Назад',
);
