<?php

return array(
    'Something went wrong. We fix it as soon as possible. Thank you for being with us.' => 'Что-то пошло не так. Мы исправим это как можно скорее. Спасибо, что Вы остаетесь с нами.',
);
