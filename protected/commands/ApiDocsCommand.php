<?php 

class ApiDocsCommand extends CConsoleCommand
{
    const DOCS_RAW     = 'raw';
    const DOCS_REDMINE = 'redmine';

    public $type = self::DOCS_REDMINE;

    private $_classInfo = null;
    private $_block = false;

    public function actionHelp()
    {
        echo "Usage: protected/yiic apidocs generate\n";
        echo " generate [--type=raw|redmine] [--controller=]";
    }

    public function actionPhpDocSyntax()
    {
        echo "Кастомный синтаксис PhpDoc используемый в проекте:
PhpDocs класса:
    НазваниеКласса Текст после НазванияКласса будет выведен как Заголовок.
    @autogen_api                    Обязательный тэг для класса подготовленного под генерацию
    @wiki_uri                       Обязательный тэг.
    @additional {{{                 Блок текста с форматированием для Redmine Wiki. Выводится сразу после заголовка.
PhpDocs метода:
    НазваниеМетода Текст после НазваниеМетода будет выведен как Заголовок.
    @deprecated [description]       Устаревший метод
    @wsse                           Необходима авторизация WSSE
    @uri uri                        Кастомный запрос URI, по умолчанию используется <controller>/<action>

    @post type name description     Аттрибут передаваемый в POST запросе
        - type          Тип получаемых данных, могут быть массивом с постфиксом [] и/или разных типов перечисленных через |
        - name          Имя аргумента
        - description   Описание аргумента

    @get  type name description     Аттрибут передаваемый в POST запросе
        - type          Тип получаемых данных, могут быть массивом с постфиксом [] и/или разных типов перечисленных через |
        - name          Имя аргумента
        - description   Описание аргумента

    @http_response_code nnn description     Описание ответа от api по коду
        - nnn           Трехзначный Код ответа
        - description   Описание Кода ответа
    @access access                  Уровень доступа к методу
    @return type                    Типа возвращаемый методом

    @example JSON {{{               Пример возвращаемых данных c типом JSON ограниченный {{{ и }}}
        ...
    }}}
";
    }

    public function actionGenerate($type = self::DOCS_REDMINE, $controller = NULL)
    {
        if ($controller == NULL) {
            echo "Parsing all controllers not implemented yet\n";
            echo "Usage: ./protected/yiic apidocs generate --controller=<controller>\n";
            return;
        }

        //$restCtrl = Yii::app()->createController($controller);
        include_once(Yii::getPathOfAlias('application.controllers.' . ucfirst($controller).'Controller').'.php');
        $info = $this->_classInfo = new ReflectionClass(ucfirst($controller).'Controller');

        if (method_exists($this, 'generate' . ucfirst($controller) . ucfirst($this->type) . 'Docs')) {
            call_user_func(array($this, 'generate' . ucfirst($controller) . ucfirst($this->type) . 'Docs'), $info);
        } else {
            echo "No dumper defined\n";
        }
    }

    protected $config = array(
            'redmine' => array(), 
    );

    // protected startBlock(blockName,type) {{{ 
    /**
     * startBlock
     * 
     * @param mixed $blockName 
     * @param mixed $type 
     * @access protected
     * @return void
     */
    protected function startBlock($blockName, $type)
    {
        //echo __METHOD__ . "\n";
        $this->_block = array(
            $blockName => array(
                $type => '',
            ),
        );
        // Return to CMap::mergeArray
        return array();
    }
    // }}}
    // protected pushBlockData(str) {{{ 
    /**
     * pushBlockData
     * 
     * @param mixed $str 
     * @access protected
     * @return void
     */
    protected function pushBlockData($str)
    {
        //echo __METHOD__ . "\n";
        if (is_array($this->_block)) {
            list($blockName, $types) = each($this->_block);
            list($type, $content) = each($types);
            //var_dump("PUSH explode: ", $blockName, $types, $type, $str);
            //$this->_block[$blockName][$type] .= trim($str) . "\n";
            $this->_block[$blockName][$type] = trim($content . $str) . "\n";
            //var_dump("PUSH:" , $str);
            reset($this->_block);
        }
    }
    // }}}
    // protected endBlock() {{{ 
    /**
     * endBlock
     * 
     * @access protected
     * @return void
     */
    protected function endBlock()
    {
        //echo __METHOD__ . "\n";
        $data = $this->_block;
        $this->_block = false;
        return $data;
    }
    // }}}

    // protected parseDocs(methodName,rawDocs,replaces) {{{ 
    /**
     * parseDocs
     * 
     * @param mixed $methodName 
     * @param mixed $rawDocs 
     * @param mixed $replaces 
     * @access protected
     * @return void
     */
    protected function parseDocs($methodName, $rawDocs, $replaces)
    {
        $skip = array(
                '/**' => true,
                '*/'  => true,
                '*'   => true,
        );

        $docs = array(
            'uri' => '/api/' . lcfirst(substr($methodName, 6)),
        );
        $desc = array();
        foreach ($rawDocs as $s) {
            $s = trim($s);
            if (isset($skip[$s])) {
                continue;
            }
            $s = preg_replace('/^\* /', '', $s);
            //echo "$s\n";
            $bufdesc = $s;
            foreach($replaces as $k => $r) {
                if (preg_match($r['pattern'], $s) > 0) {
                    $bufdesc = false;
                    if (!isset($r['pattern']) && (!isset($r['func']) || !isset($r['replacement']))) {
                        echo "Undefinded 'pattern' or 'replacement' for \$replaces[$k]";
                    }

                    if (isset($r['replacement']) && is_array($r['replacement'])) {
                        $call = (isset($r['replacement']) && is_array($r['replacement'])) ? $r['replacement'] : $r['func'];
                        $fName = $call[0];
                        unset($call[0]);
                        $call = array_values($call);
                        //echo "$fName[1]\n";
                        //var_dump("FuncCall: ". get_class($fName[0]) . '::' . $fName[1], $part, array_values($call));
                        $s = call_user_func($fName, $s, $methodName);
                        if (isset($r['parseOnly'])) {
                            $bufdesc = $s;
                            continue;
                        } else {
                            break;
                        }
                    }

                    if (isset($r['func'])) {
                        $call = (isset($r['replacement']) && is_array($r['replacement'])) ? $r['replacement'] : $r['func'];
                        $fName = $call[0];
                        unset($call[0]);
                        $call = array_values($call);
                        //echo "$fName[1]\n";
                        //var_dump("FuncCall: ". get_class($fName[0]) . '::' . $fName[1], $part, array_values($call));
                        if (!isset($r['parseOnly'])) {
                            $part = call_user_func_array($fName, $call);
                            $docs = CMap::mergeArray($docs, $part);
                            break;
                        } else {
                            //$s = preg_replace($r['pattern'], $call[0], $s);
                            $s = call_user_func($fName, $s, $methodName);
                            //
                            continue;
                        }
                    }

                    if ($this->_block !== false) {
                        continue;
                    }
                    $s = preg_replace($r['pattern'], $r['replacement'], $s);

                    if (isset($r['key'])) {
                        if (isset($r['array'])) {
                            $docs[$r['key']][] = $s;
                            $s = false;
                            break;
                        } else {
                            $docs[$r['key']] = $s;
                            $s = false;
                            break;
                        }
                    //} else {
                        //$docs[] = $s;
                        //break;
                    }
                }
            }

            if ($bufdesc) {
                if ($this->_block !== false) {
                    $this->pushBlockData($s);
                } else {
                    $desc[] = $s;
                }
            }
        }

        if ($this->_block !== false) {
            list($blockName, $types) = each($this->_block);
            list($type, $content) = each($types);
            fprintf(STDERR, "Незакрытый блок @" . $blockName . " " . $type . " у " . $methodName . "!!!\n");
        }

        $docs['description'] = implode("\n", $desc);
        return $docs;
    }
    // }}}

    // protected parseMethodDocs(methodName,doc) {{{ 
    /**
     * parseMethodDocs
     * 
     * @param mixed $methodName 
     * @param mixed $doc 
     * @access protected
     * @return void
     */
    protected function parseMethodDocs($methodName, $doc)
    {
        $rawDocs = explode("\n", $doc);
        $replaces = array(
                array(
                    'pattern' => '/{@enum ([^}]+)}/',
                    'replacement' => array(array($this, 'enumReveal')),
                    'parseOnly' => true,
                ),
                array(
                    'key' => 'title',
                    'pattern' => '/^action(' . substr($methodName, 6) . ')$/',
                    'replacement' => '\1',
                ),
                array(
                    'key' => 'title',
                    'pattern' => '/^' . $methodName . ' (.*)$/',
                    'replacement' => '\1',
                ),


                array(
                    'key' => 'deprecated',
                    'pattern' => '/^@deprecated$/',
                    'replacement' => '',
                ),
                array(
                    'key' => 'deprecated',
                    'pattern' => '/^@deprecated (.*)$/',
                    'replacement' => '\1',
                ),

                array(
                    'key' => 'wiki_uri',
                    'pattern' => '/^@wiki_uri (.*)$/',
                    'replacement' => '\1',
                ),
                array(
                    'key' => 'uri',
                    'pattern' => '/^@uri (.*)$/',
                    'replacement' => '\1',
                ),

                array(
                    'key' => 'auth',
                    'pattern' => '/^@wsse$/',
                    'replacement' => 'Требуется авторизация по WSSE заголовку',
                ),

                array(
                    'key' => 'get',
                    'pattern' => '/^@get ([^[:blank:]]+) ([^[:blank:]]+)$/',
                    'replacement' => '\'\2\' c типом \1',
                    'array' => true,
                ),
                array(
                    'key' => 'get',
                    'pattern' => '/^@get ([^[:blank:]]+) ([^[:blank:]]+) (.*)$/',
                    'replacement' => '\1 *\2* : \3',
                    'array' => true,
                ),

                array(
                    'key' => 'post',
                    'pattern' => '/^@post ([^[:blank:]]+) ([^[:blank:]]+)$/',
                    'replacement' => '\'*\2*\' с типом \1',
                    'array' => true,
                ),
                array(
                    'key' => 'post',
                    'pattern' => '/^@post ([^[:blank:]]+) ([^[:blank:]]+) (.*)$/',
                    'replacement' => '\1 *\2* : \3',
                    'array' => true,
                ),

                array(
                    'key' => 'access',
                    'pattern' => '/^@access (.*)$/',
                    'replacement' => '',
                ),

                array(
                    'key' => 'http_response_code',
                    'pattern' => '/^@http_response_code ([0-9]+) (.*)$/',
                    'replacement' => '*\1* = \2',
                    'array' => true,
                ),
                array(
                    'key' => 'return',
                    'pattern' => '/^@return void$/',
                    'replacement' => '',
                ),
                array(
                    'key' => 'return',
                    'pattern' => '/^@return (.*)$/',
                    'replacement' => 'Возвращаемое значение с типом \1',
                ),

                array(
                    'key' => 'autogen_ready',
                    'pattern' => '/^@autogen_api$/',
                    'replacement' => 'Подготовлен под Mindmeal ApiDocs Generator',
                ),

                // BLOCKS
                array(
                    'key' => 'example',
                    'pattern' => '/^@example JSON {{{$/',
                    'func' => array(array($this, 'startBlock'), 'example', 'JSON'),
                ),
                array(
                    'key' => 'additional',
                    'pattern' => '/^@additional {{{$/',
                    'func' => array(array($this, 'startBlock'), 'additional', 'TEXT'),
                ),

                array(
                    'pattern' => '/^}}}$/',
                    'func' => array(array($this, 'endBlock')),
                ),

        );
        return $this->parseDocs($methodName, $rawDocs, $replaces);
    }
    // }}}

    // protected enumReveal(string,className) {{{ 
    /**
     * enumReveal
     * 
     * @param mixed $string 
     * @param mixed $className 
     * @access protected
     * @return void
     */
    protected function enumReveal($string, $className)
    {
        preg_match_all('/{@enum ([^}]+)}/', $string, $matches);
        $class = explode("::", $matches[1][0]);
        if (($class[0] == 'self')) {
            $info = $this->_classInfo;
        } else {
            $info = new ReflectionClass($class[0]);
        }

        if (preg_match('/[A-Z\_0-9]+[*]/', $class[1])) {
            $values = array();
            $matching = '/'.str_replace('*', '.*', $class[1]).'/';
            foreach($info->getConstants() as $const => $value) {
                if (preg_match($matching, $const) > 0) {
                    $values[] = "*_" . $value . "_*";
                }
            }
            $replace = implode(' | ', $values);
        } else {
            fwrite(STDERR, "Wrong enum format\n");
        }

        return str_replace($matches[0][0], $replace, $string);
    }
    // }}}

    // protected generateRestRedmineDocs(info) {{{ 
    /**
     * generateRestRedmineDocs
     * 
     * @param mixed $info 
     * @access protected
     * @return void
     */
    protected function generateRestRedmineDocs($info) {
        $docs = array();

        $titles = array(
                'get' => 'Передаваемый аргумент GET:',
                'post' => 'Передаваемый аргумент POST:',
                'example' => 'Пример возвращаемого ответа %s:',
                'additional' => '',
                'http_response_code' => 'Описание кодов возвращаемых ответов:',
        );
        $additional = $info->getDocComment();
        if ($additional !== false) {
            // @ak: parentDocs
            $additional = $this->parseMethodDocs($info->name, $additional);
            if (!isset($additional['autogen_ready'])) {
                fprintf(STDERR, "!!! Класс " . $info->name . " не подготовлен под автогенерацию\n");
                $this->actionHelp();
            }
            if (isset($additional['title'])) {
                echo "h1. " . $additional['title'] . "\n";
                echo "\n";
            }
        }

        foreach($info->getMethods() as $m) {
            if ((strpos($m->name, 'action') === 0) && (strcmp($m->getDeclaringClass()->name, $info->name) === 0)) {
                $doc = $m->getDocComment();
                if ($doc === false) {
                    fprintf(STDERR, 'Нет описания для ' . $m->name . "\n");
                    continue;
                }
                $docs[] = $this->parseMethodDocs($m->name, $doc);
            }
        }

        if (isset($additional['additional']['TEXT'])) {
            echo $additional['additional']['TEXT'] . "\n";
        }

        echo "Список предоставляемых API:\n";
        foreach ($docs as $d) {
            echo "* [[" . (isset($additional['wiki_uri']) ? $additional['wiki_uri'] : '') . "#" . $d['title'] . "|" . $d['title'] . "]]";
            if (isset($d['deprecated'])) {
                echo " _устарел_";
            }
            echo "\n";
        }
        echo "\n\n";


        foreach ($docs as $d) {
            echo "----\n\n";
            echo "h2. " . $d['title'] . "\n";
            echo "\n";
            if (isset($d['deprecated'])) {
                echo "*Метод устарел. " . $d['deprecated'] . "*\n";
            }
            if (!empty($d['description'])) {
                echo "bq. " . $d['description'] . "\n\n";
            }
            echo "URI запроса: *" . $d['uri'] . "*\n";
            if (isset($d['auth'])) {
                echo "*" . $d['auth'] . "*\n";
            }
            echo "";
            if (isset($d['get'])) {
                if (isset($titles['get'])) {
                    echo $titles['get'] . "\n";
                }
                foreach($d['get'] as $p) {
                    echo "* " . $p . "\n";
                }
                echo "\n";
            }
            if (isset($d['post'])) {
                if (isset($titles['post'])) {
                    echo $titles['post'] . "\n";
                }
                foreach($d['post'] as $p) {
                    echo "* " . $p . "\n";
                }
                echo "\n";
            }

            if (isset($d['http_response_code'])) {
                if (isset($titles['http_response_code'])) {
                    echo $titles['http_response_code'] . "\n";
                }
                foreach($d['http_response_code'] as $p) {
                    echo "* " . $p . "\n";
                }
                echo "\n";
            }

            if (isset($d['example'])) {
                list($type, $content) = each($d['example']);
                if (isset($titles['example'])) {
                    echo sprintf($titles['example'], $type) . "\n";
                }

                echo "<pre>" . $content . "</pre>\n";
            }
            //var_dump($d);
            echo "\n\n";
        }
    }
    // }}}
}
