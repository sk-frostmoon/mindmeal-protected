<div class="container">
    <div id="activationbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel <?= $model->hasErrors() ? 'panel-danger' : 'panel-info'?>" >
                <div class="panel-heading">
                    <div class="panel-title"><?=Yii::t('secure', 'Account activation');?></div>
                </div>

                <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                        <? if ($model->hasErrors()) { ?>
                        <div style="margin-bottom: 25px" class="input-group">
                            <?
                                $errors = $model->getErrors();
                                echo implode('<br/>', $errors['activated']);
                            ?>
                        </div>
                        <? } else { ?>
                        <div style="margin-bottom: 25px" class="input-group">
                            <?=Yii::t('secure', 'Your account was been activated. Now you can try to login.');?>
                        </div>
                        <? } ?>

                        <div style="margin-top:25px; text-align: center;" class="form-group">
                            <!-- Button -->
                            <div class="col-sm-12 controls">
                                <a id="btn-login" href="<?=Yii::app()->createUrl('/user/login');?>" class="btn btn-success"><?=Yii::t('secure', 'OK');?></a>
                            </div>
                        </div>
                </div>
        </div>
    </div>
    <script type="text/javascript">
        function sendState(state) {
            window.onload = function () {
                var w       = window;
                var sended  = false;
                var data    = {
                    'state': state,
                    'resize': {
                        h: $('#activationbox').height(),
                        w: $('#activationbox').width(),
                    }
                };

                while ((w = w.parent) && (window.top !== w)) {
                    w.postMessage(data, '*');
                    sended = true;
                }
                if (!sended) {
                    w.postMessage(data, '*');
                }
            };
        }
        sendState('activation_sended');
    </script>
</div>

