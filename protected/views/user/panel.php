<?
$path = Yii::app()->theme->baseUrl . "/assets";
?>
<header id="header">
    <div class="wrap clearfix">
        <div class="logo"><a href="/"><img src="<?=$path;?>/img/header-logo.png" alt="mind meal logo" /></a></div>
        <nav class="nav">
            <ul>
                <li><span>привет, <span><?=($user instanceof UserModel) ? $user->username : Yii::app()->user->guestName;?>!</span></span><img src="<?=($user instanceof UserModel) ? $user->getAvatarUrl('38x38') : '/images/avatars/default.png';?>" height="35" alt="avatar menu" /></li>
                <li><a href="<?=$this->createUrl('/user/profile');?>" class="profile">Профиль</a></li>
                <li><a href="<?=$this->createUrl('/support/');?>" class="support">Поддержка</a></li>
                <? /*<!--<li><a href="#" class="cash">5100$</a></li --> */ ?>
                <li><a href="<?=$this->createUrl('/user/edit');?>" class="settings">Настройки</a></li>
                <li><a href="<?=$this->createUrl('/user/logout');?>" class="exit">Выход</a></li>
            </ul>
        </nav>
    </div>
</header>
