
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'forgottenform',
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                        'htmlOptions' => array(
                            'class' => 'reg-form',
                            'role' => 'form'
                        )
                    )); ?>

                    <h1><?=Yii::t('secure', 'Restore password');?></h1>


                        <?php echo $form->textField($model, 'email',
                            array(
                                'class' => 'w100',
                                'placeholder' => $model->getAttributeLabel('email'),
                                'id' => 'login-email'
                            )
                        ); ?>

                    <div style="margin-bottom: 10px; width: 100%;" class="input-group">
                        <?php echo $form->error($model,'email'); ?>
                    </div>

                    <div class="buttons clearfix">
                        <input type="submit" name="login" class="w100" value="<?=Yii::t('secure', 'Restore password');?>" />
                    </div>
                <?php $this->endWidget(); ?>
