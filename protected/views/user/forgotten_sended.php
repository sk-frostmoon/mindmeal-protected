<div class="reg-form">
    <div class="panel-heading">
        <div class="panel-title">Письмо отправлено!</div>
    </div>

    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

    <div style="margin-bottom: 25px" class="input-group">
        Письмо с ссылкой для восстановления пароля выслано на указанный адрес.
    </div>

    <div class="buttons clearfix">
        <input type="submit" class="w100" onclick="window.location.href = '<?=$this->createUrl('/user/login');?>'; return false;" value="ок"/>
    </div>
</div>