<? if ($model->getIsNewRecord()) { ?>
    <div class="reg-form">
        <div class="panel-heading">
            <div class="panel-title">Ошибка</div>
        </div>

        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

        <div style="margin-bottom: 25px" class="input-group">
            Токен устарел
        </div>

        <div class="buttons clearfix">
            <input type="submit" class="w100" onclick="window.location.href = '<?=$this->createUrl('/user/login');?>'; return false;" value="ок"/>
        </div>
    </div>
<? } else { ?>
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'restoreform',
        'action' => $this->createUrl('/user/restore'),
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
        'htmlOptions' => array(
            'class' => 'reg-form',
            'role' => 'form'
        )
    )); ?>

    <h1>Укажите новый пароль</h1>

    <input type="hidden" name="token" value="<?= $model->getToken(); ?>"/>


        <?php echo $form->passwordField($model, 'password',
            array(
                'class' => 'w100',
                'data-focus' => $model->getAttributeLabel('password'),
                'placeholder' => $model->getAttributeLabel('password'),
                'id' => 'login-password',
                'required' => true,
            )
        ); ?>
        <!-- span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span -->
        <?php echo $form->passwordField($model, 'passwordConfirm',
            array(
                'class' => 'w100',
                'data-focus' => $model->getAttributeLabel('passwordConfirm'),
                'placeholder' => $model->getAttributeLabel('passwordConfirm'),
                'id' => 'login-password-confirm',
                'required' => true,
            )
        ); ?>

    <div style="margin-bottom: 25px; text-align: center; display: block;" class="input-group">
        <?php echo $form->error($model,'password'); ?>
        <?php echo $form->error($model,'passwordConfirm'); ?>
    </div>

    <div class="buttons clearfix">
        <input type="submit" name="login" class="w100" value="Сохранить" />
    </div>
    <?php $this->endWidget(); ?>

    <script type="text/javascript" src="/js/placeholder.js"></script>
<? } ?>
