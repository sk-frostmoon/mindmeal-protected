<?
$path = Yii::app()->theme->baseUrl . "/assets";
$cs = Yii::app()->clientScript;
$cs->registerCSSFile($this->createUrl($path . '/css/style_small.css'));


$hasErrors = $user->getErrors();
if ( (!empty($hasErrors)) && (isset($hasErrors['passwordOld']) || isset($hasErrors['password']) || isset($hasErrors['passwordConfirm'])) ) {
    $cs->registerScript('pw-modal-errors', '$(\'#pw-modal\').modal(\'show\');');
}

if (Yii::app()->user->hasFlash('notice')) {
    $cs->registerScript('notice-modal',
        '$(\'#notice-modal\')'
        . '.modal(\'show\')'
        . '.on(\'shown.bs.modal\', function () { window.setTimeout(function () {$(\'#notice-modal\').modal(\'hide\');}, 2000); });'
    );
}
?>
<div class="heading-bg money">
    <div class="wrap">
        <h1>Финансы</h1>
    </div>
</div>
<div id="profile" class="wrap clearfix">
    <div class="left-col">
        <div class="profile-info">
            <div class="profile-img">
                <form id="uploader" class="profile-avatar col-md-4" style="overflow: hidden;">
                    <img src="<?=$user->getAvatarUrl('230x230');?>" style="max-width: 230px;"/>
                    <input id="avatarUploader" type="file" name="avatar" style="opacity: 0; position: absolute; z-index: 11;" class="btn input-group" onchange="javascript: changeAvatarCrop(this);"/>
                    <a href="#" onclick="return false;">
                        Поменять аватар
                    </a>
                </form>
            </div>
            <div class="profile-nickname">
                <span class="nickname"><?=$user->username;?></span>
                <span class="name"><?=$user->fname;?> <?=$user->lname;?></span>
                <!--<span class="last-network"><span>Был в сети:</span> Вт, 17 сентября 2014г. 22:48</span>-->
            </div>
            <div>
                <h2>Выбери игру</h2>
                <a href="/fd/"  alt="Final Desire"  title="Final Desire"><img src="/themes/mm_secure/assets/img/fd-profile.jpg" alt="Final Desire"/></a>
                <a href="/solgame/"  alt="Shards of Light"  title="Shards of Light"><img src="/themes/mm_secure/assets/img/sol-profile.jpg" alt="Shards of Light"/></a>
            </div>
        </div>
    </div>
     <div id="money" class="main">
        <div class="buttons clearfix">
            <h2 style="float: left;margin-top: 2px;">Баланс вашего счета: <span style="font-size: 26px;position: relative;top: 2px;padding: 0 10px;margin-left: 10px;background: #f6f7f8;">5100$</span></h2>
            <button style="float: right; background: #e7493a; width:250px;">Пополнить счет</button>
        </div>
        <h2>История операций:</h2>
        <table>
                <thead>
                    <tr>
                        <td>№</td>
                        <td>Вид</td>
                        <td>Название</td>
                        <td>Дата</td>
                        <td>Сумма</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td><span class="status green"></span></td>
                        <td><span class="act">Пополнение</span> с карты 4765 6563 4555 6536</td>
                        <td>02.09.2014</td>
                        <td>250$</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><span class="status red"></span></td>
                        <td><span class="act">Списание</span> - ежемесячный платеж</td>
                        <td>02.09.2014</td>
                        <td>350$</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><span class="status green"></span></td>
                        <td><span class="act">Оплата</span> заказа №14545</td>
                        <td>02.09.2014</td>
                        <td>450$</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><span class="status green"></span></td>
                        <td><span class="act">Пополнение</span> с карты 4765 6563 4555 6536</td>
                        <td>02.09.2014</td>
                        <td>250$</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><span class="status red"></span></td>
                        <td><span class="act">Списание</span> - ежемесячный платеж</td>
                        <td>02.09.2014</td>
                        <td>350$</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><span class="status green"></span></td>
                        <td><span class="act">Оплата</span> заказа №14545</td>
                        <td>02.09.2014</td>
                        <td>450$</td>
                    </tr>
                </tbody>
        </table>
        <div class="buttons" style="text-align:right;">
            <button type="button" style="margin-right:0;width:250px;">все операции</button>
        </div>
     </div>


</div>
<!-- !!!!!!!!!!!!!!!!!!! -->
<div id="crop-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <b>Сохранение аватара</b>
            </div>
            <form>
                <input type="hidden" id="cropped_image" value=""/>
                <div class="modal-body">
                    <div id="filesInfo" style="width: 600px; margin-bottom: 10px;">
                    </div>
                    <div class="thumbsnail pull-left" style="width: 600px;">
                        <div class="thumbs200 pull-left" style="margin-right: 10px;">
                            <canvas id="t200" width="200" height="200"></canvas>
                        </div>
                        <div class="thumbs100 pull-left" style="margin-right: 10px;">
                            <canvas id="t100" width="100" height="100"></canvas>
                        </div>
                        <div class="thumbs50 pull-left">
                            <canvas id="t50" width="50" height="50"></canvas>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="buttons clearfix">
                    <button class="btn btn-danger" data-dismiss="modal" type="button">Отменить</button>
                    <button class="btn btn-primary pull-left" type="submit">Изменить аватар</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ------------ -->

<? if (Yii::app()->user->hasFlash('notice')) { ?>
    <div id="notice-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-info">
            <div class="modal-content">
                <div class="modal-header bg-aqua">
                    <b>Notice</b>
                </div>
                <div class="modal-body">
                    <p class="text"><?=Yii::app()->user->getFlash('notice');?></p>
                </div>
            </div>
        </div>
    </div>
<? } ?>
