<?php
$cs = Yii::app()->clientScript;

ob_start();
?>
    $(document).ready(function() {
        <? if (Yii::app()->request->getParam('force_redirect', false) === false) { // @ak: for debug and styling ?>
            _mm.push('loginRegCounter', {utm_user: <?=intval($user->id)?>});
            window.setTimeout(function () {
                $('#registration_finish').submit();
            }, 3000);
        <? } else { ?>
            $('#registration_finish').submit();
        <? } ?>
    });
<?
$cs->registerScript('register_greetings', ob_get_clean());
?>
<div class="greetings auth-form">
    <? $form = $this->beginWidget('CActiveForm', array(
        'id' => 'registration_finish',
        'action' => preg_replace('/\/$/', '', $user->host) . '/login',
        'method' => 'POST',
    )); ?>
        <input type="hidden" name="firstLogin" value="1"/>
        <?=$form->hiddenField($model, 'email'); ?>
        <?=$form->hiddenField($model, 'password'); ?>
        <h2>Подождите, идет авторизация на сервисе...</h2>
    <? $this->endWidget(); ?>
    <noscript>
        <div class="noscript">
            Мы обнаружили, что JavaScript не включен.<br/>
            Чтобы продолжить, пожалуйста, внесите нас в свой белый список или включите поддержку JavaScript.
        </div>
    </noscript>
</div>
