<?
$path = Yii::app()->theme->baseUrl . "/assets";
$cs = Yii::app()->clientScript;
$cs->registerCSSFile($this->createUrl($path . '/css/style_small.css'));
?>
<div class="heading-bg helpdesk">
    <div class="wrap">
        <h1>Новый запрос</h1>
    </div>
</div>
<div class="wrap clearfix">
    <div class="main my-requests">
        <h1>название запроса может быть достаточно длинным, поэтому авоыраофалофывфывфыв</h1>
        <div class="request">
            <ul>
                <li><span>ID-номер запроса:</span>LMA-V16-Y882 (Запрос номер: 1)</li>
                <li><span>Статус запроса:</span>Ожидает ответа от службы поддержки [Отметить как решенный]</li>
                <li><span>Создан:</span>2014-07-04 15:55:35</li>
                <li><span>Обновлен:</span>2014-07-07 15:17:45</li>
                <li><span>Последний ответ:</span>Kakoytoher</li>
                <li><span>Категория:</span>Тупые вопросы</li>
                <li><span>Ответов:</span>5</li>
                <li><span>Приоритет:</span>Низкий</li>
            </ul>
        </div>

        <div class="response">
            <div class="item">
                <span class="print"></span>
                <ul>
                    <li><span>Дата:</span>2014-07-04 15:55:35</li>
                    <li><span>Имя:</span>kakoytoher</li>
                    <li><span>Email:</span>kakoytoher@gmail.com</li>
                </ul>
                <div class="message">
                    <span>Сообщение:</span>
                    <p>
                        Товарищи! постоянное информационно-пропагандистское обеспечение нашей
                        деятельности требуют от нас анализа модели развития. С другой стороны
                         постоянный количественный рост и сфера нашей активности требуют
                         определения и уточнения новых предложений.
                    </p>
                </div>
            </div>
            <div class="item">
                <span class="print"></span>
                <ul>
                    <li><span>Дата:</span>2014-07-04 15:55:35</li>
                    <li><span>Имя:</span>kakoytoher</li>
                    <li><span>Email:</span>kakoytoher@gmail.com</li>
                </ul>
                <div class="message">
                    <span>Сообщение:</span>
                    <p>
                        Товарищи! постоянное информационно-пропагандистское обеспечение нашей
                        деятельности требуют от нас анализа модели развития. С другой стороны
                         постоянный количественный рост и сфера нашей активности требуют
                         определения и уточнения новых предложений.
                    </p>
                </div>
            </div>
            <div class="item">
                <span class="print"></span>
                <ul>
                    <li><span>Дата:</span>2014-07-04 15:55:35</li>
                    <li><span>Имя:</span>kakoytoher</li>
                    <li><span>Email:</span>kakoytoher@gmail.com</li>
                </ul>
                <div class="message">
                    <span>Сообщение:</span>
                    <p>
                        Товарищи! постоянное информационно-пропагандистское обеспечение нашей
                        деятельности требуют от нас анализа модели развития. С другой стороны
                         постоянный количественный рост и сфера нашей активности требуют
                         определения и уточнения новых предложений.
                    </p>
                </div>
            </div>
        </div>
        <h2>Ответить</h2>
        <form action="#" method="post" enctype="multipart/form-data" name="form1">
            <textarea name="text" placeholder="ТЕКСТ ОТВЕТА:"></textarea>
            <div class="wrap clearfix">
                <div class="upload-info">
                    <span class="title">Прикрепленные файлы:</span>
                    <span class="limits">Лимиты для загруженных файлов</span>
                </div>
                <div class="bt-file">
                    <span>Выбрать файл</span>
                        <input type="file" size="1"  name="file" id="file">
                </div>
                <input type="submit" value="Отправить запрос"/>
            </div>
        </form>
    </div>

</div>



<!-- !!!!!!!!!!!!!!!!!!! -->
<div id="crop-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <b>Сохранение аватара</b>
            </div>
            <form>
                <input type="hidden" id="cropped_image" value=""/>
                <div class="modal-body">
                    <div id="filesInfo" style="width: 600px; margin-bottom: 10px;">
                    </div>
                    <div class="thumbsnail pull-left" style="width: 600px;">
                        <div class="thumbs200 pull-left" style="margin-right: 10px;">
                            <canvas id="t200" width="200" height="200"></canvas>
                        </div>
                        <div class="thumbs100 pull-left" style="margin-right: 10px;">
                            <canvas id="t100" width="100" height="100"></canvas>
                        </div>
                        <div class="thumbs50 pull-left">
                            <canvas id="t50" width="50" height="50"></canvas>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="buttons clearfix">
                    <button class="btn btn-danger" data-dismiss="modal" type="button">Отменить</button>
                    <button class="btn btn-primary pull-left" type="submit">Изменить аватар</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ------------ -->

<? if (Yii::app()->user->hasFlash('notice')) { ?>
    <div id="notice-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-info">
            <div class="modal-content">
                <div class="modal-header bg-aqua">
                    <b>Notice</b>
                </div>
                <div class="modal-body">
                    <p class="text"><?=Yii::app()->user->getFlash('notice');?></p>
                </div>
            </div>
        </div>
    </div>
<? } ?>
