<?
$path = Yii::app()->theme->baseUrl . "/assets";
$cs = Yii::app()->clientScript;
$cs->registerCSSFile($this->createUrl($path . '/css/style_small.css'));
?>
<div class="heading-bg helpdesk">
    <div class="wrap">
        <h1>Новый запрос</h1>
    </div>
</div>
<div class="wrap clearfix">
    <div class="main my-tickets">
        <h1>Посмотреть мой запрос</h1>
        <form>
        <div class="row">
            <input type="text" placeholder="ID-НОМЕРА ЗАПРОСА" />
        </div>
        <div class="row">
            <input type="submit" value="Посмотреть запрос" />
        </div>
            <a href="#">Забыл ID-номер?</a>
        </form>
    </div>
</div>



<!-- !!!!!!!!!!!!!!!!!!! -->
<div id="crop-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <b>Сохранение аватара</b>
            </div>
            <form>
                <input type="hidden" id="cropped_image" value=""/>
                <div class="modal-body">
                    <div id="filesInfo" style="width: 600px; margin-bottom: 10px;">
                    </div>
                    <div class="thumbsnail pull-left" style="width: 600px;">
                        <div class="thumbs200 pull-left" style="margin-right: 10px;">
                            <canvas id="t200" width="200" height="200"></canvas>
                        </div>
                        <div class="thumbs100 pull-left" style="margin-right: 10px;">
                            <canvas id="t100" width="100" height="100"></canvas>
                        </div>
                        <div class="thumbs50 pull-left">
                            <canvas id="t50" width="50" height="50"></canvas>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="buttons clearfix">
                    <button class="btn btn-danger" data-dismiss="modal" type="button">Отменить</button>
                    <button class="btn btn-primary pull-left" type="submit">Изменить аватар</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ------------ -->

<? if (Yii::app()->user->hasFlash('notice')) { ?>
    <div id="notice-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-info">
            <div class="modal-content">
                <div class="modal-header bg-aqua">
                    <b>Notice</b>
                </div>
                <div class="modal-body">
                    <p class="text"><?=Yii::app()->user->getFlash('notice');?></p>
                </div>
            </div>
        </div>
    </div>
<? } ?>
