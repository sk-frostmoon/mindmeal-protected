<div class="container">
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title"><?=Yii::t('secure', 'Вы авторизованы');?></div>
            </div>

            <div style="padding-top:30px; text-align: center;" class="panel-body" >
                Вы авторизованы как <?=Yii::app()->user->getName();?>
                <br /><br />
                <a id="btn-logout" href="/user/logout/" class="btn btn-success"><?=Yii::t('secure', 'Выйти');?></a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function sendState(state) {
            window.onload = function () {
                var w       = window;
                var sended  = false;
                var data    = {
                    'state': state,
                    'resize': {
                        h: $('#loginbox').height(),
                        w: $('#loginbox').width(),
                    }
                };

                while ((w = w.parent) && (window.top !== w)) {
                    w.postMessage(data, '*');
                    sended = true;
                }
                if (!sended) {
                    w.postMessage(data, '*');
                }
            };
        }
        sendState('<?=Yii::app()->user->isGuest ? 'unauthorized' : 'authorized'?>');
    </script>
</div>
