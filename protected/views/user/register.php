<?php

$cs = Yii::app()->clientScript;

if (YII_DEBUG) {
    $cs->registerScript('demo_data', '
            var regUserId = "removeMe" + Math.random().toString().substr(-8);
            $("#login-username").val(regUserId);
            $("#login-email").val(regUserId + "@mindmeal.ru")
            $("#login-password").val("asdfasdf");
            $("#login-password-confirm").val("asdfasdf");
    ');
}

$form = $this->beginWidget('CActiveForm', array(
    'id'=>'registrationform',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'reg-form',
        'role' => 'form'
    )
)); ?>

    <h1>регистрация mind meal</h1>

    <? if (isset($url)) { ?>
        <input type="hidden" name="url" value="<?=CHtml::encode((string)$url);?>"/>
    <? } ?>

    <?= $form->textField($model, 'username',
        array(
            'class' => 'w100',
            'data-focus' => $model->getAttributeLabel('username'),
            'placeholder' => $model->getAttributeLabel('username'),
            'id' => 'login-username',
        )
    ); ?>

    <div style="margin-bottom: 0px; text-align: center; display: block;" class="input-group">
        <?php echo $form->error($model,'username'); ?>
    </div>


    <?//if (defined('IS_REST_REQUEST') and IS_REST_REQUEST) {
    //    echo $form->hiddenField($model, 'host');
    //} ?>

    <?= $form->textField($model, 'email',
        array(
            'class' => 'w100',
            'data-focus' => $model->getAttributeLabel('email'),
            'placeholder' => $model->getAttributeLabel('email'),
            'id' => 'login-email',
            'required' => true,
        )
    ); ?>
    <div style="margin-bottom: 25px; text-align: center; display: block;" class="input-group">
        <span class="small"><?php echo $form->error($model,'email'); ?></span>
    </div>


    <?php echo $form->passwordField($model, 'password',
        array(
            'class' => 'w100',
            'data-focus' => $model->getAttributeLabel('password'),
            'placeholder' => $model->getAttributeLabel('password'),
            'id' => 'login-password',
            'required' => true,
        )
    ); ?>
    <div style="margin-bottom: 0px; text-align: center; display: block;" class="input-group">
        <span class="small"><?php echo $form->error($model,'password'); ?></span>
    </div>

        <!-- span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span -->
    <?php echo $form->passwordField($model, 'passwordConfirm',
        array(
            'class' => 'w100',
            'data-focus' => $model->getAttributeLabel('passwordConfirm'),
            'placeholder' => $model->getAttributeLabel('passwordConfirm'),
            'id' => 'login-password-confirm',
            'required' => true,
        )
    ); ?>
    <div style="margin-bottom: 25px; text-align: center; display: block;" class="input-group">
        <span class="small"><?php echo $form->error($model,'passwordConfirm'); ?></span>
    </div>


    <div class="input-group license-agreement">
        <?php 
            $params = array(
                'id' => 'licenseAgree',
                'class' => 'pull-left',
                'style' => 'margin-bottom: 15px;',
            );
            $params = CMap::mergeArray($params, (!isset($model->licenseAgree) || ($model->licenseAgree == '1')) ? array('checked'=>'checked') : array());

            echo $form->checkBox($model, 'licenseAgree', $params);
            echo " " . $form->label($model, 'licenseAgree');
        ?>
    </div>
    <? if ($model->hasErrors('licenseAgree')) { ?>
        <div style="margin-bottom: 25px; text-align: center; display: block;" class="input-group">
            <span class="small"><?php echo $form->error($model, 'licenseAgree'); ?></span>
        </div>
    <? } ?>

    <div class="buttons clearfix">
        <input type="submit" name="login" class="w100" value="Зарегистрироваться" />
    </div>

<?php $this->endWidget(); ?>

<div id="license_modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
	    <? $this->forward('/user/license', false); ?>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#calendar').simpleDatepicker({
            dateFormat: "<?=UserModel::$birthdayFormat['user'];?>"
        });
    });

</script>
<script type="text/javascript" src="/js/placeholder.js"></script>
