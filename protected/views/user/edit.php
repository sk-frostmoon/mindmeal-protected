<?
$path = Yii::app()->theme->baseUrl . "/assets";
$cs = Yii::app()->clientScript;
$cs->registerCSSFile($this->createUrl($path . '/css/style_small.css'));


$hasErrors = $model->getErrors();
if ( (!empty($hasErrors)) && (isset($hasErrors['passwordOld']) || isset($hasErrors['password']) || isset($hasErrors['passwordConfirm'])) ) {
    $cs->registerScript('pw-modal-errors', '$(\'#pw-modal\').modal(\'show\');');
}

if (Yii::app()->user->hasFlash('notice')) {
    $cs->registerScript('notice-modal',
        '$(\'#notice-modal\')'
        . '.modal(\'show\')'
        . '.on(\'shown.bs.modal\', function () { window.setTimeout(function () {$(\'#notice-modal\').modal(\'hide\');}, 2000); });'
    );
}
?>
<style>
.buttons button.w100 { width: 100%; }
.notifications {
    margin: 25px 0 0 20px;
    padding: 40px;
}
.notifications .title {
    font-family: 'roadradioregular';
    font-size: 36px;
    margin-bottom: 5px;
    text-align: center;
}
</style>
<div class="heading-bg">
    <div class="wrap">
        <h1>Настройки</h1>
    </div>
</div>
<div id="profile" class="wrap clearfix">
    <div class="left-col">
        <div class="profile-info">
            <div class="profile-img">
                <form id="uploader" class="profile-avatar col-md-4" style="overflow: hidden;">
                    <img src="<?=$model->getAvatarUrl('230x230');?>" style="max-width: 230px;"/>
                    <input id="avatarUploader" type="file" name="avatar" style="opacity: 0; position: absolute; z-index: 11;" class="btn input-group" onchange="javascript: changeAvatarCrop(this);"/>
                    <a href="#" onclick="return false;">
                        Поменять аватар
                    </a>
                </form>
            </div>
            <div class="profile-nickname">
                <span class="nickname"><?=$model->username;?></span>
                <span class="name"><?=$model->fname;?> <?=$model->lname;?></span>
                <!--<span class="last-network"><span>Был в сети:</span> Вт, 17 сентября 2014г. 22:48</span>-->
            </div>
            <div>
                <h2>Выбери игру</h2>
                <a href="/fd/"  alt="Final Desire"  title="Final Desire"><img src="/themes/mm_secure/assets/img/fd-profile.jpg" alt="Final Desire"/></a>
                <a href="/solgame/"  alt="Shards of Light"  title="Shards of Light"><img src="/themes/mm_secure/assets/img/sol-profile.jpg" alt="Shards of Light"/></a>
            </div>
        </div>
        <div class="main">

        </div>
    </div>
    <div class="right-col">
        <div class="">
            <? $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'password-change',
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                )); ?>
                <div class="title"><?=Yii::t('secure', 'Changing password');?></div>
                    <input type="password" name="Password[passwordOld]" value="" placeholder="<?=$model->getAttributeLabel('passwordOld');?>"/>
                    <?= $form->error($model, 'passwordOld');?>
                    <input type="password" name="Password[password]" value="" placeholder="<?=$model->getAttributeLabel('password');?>"/>
                    <?= $form->error($model, 'password');?>
                    <input type="password" name="Password[passwordConfirm]" value="" placeholder="<?=$model->getAttributeLabel('passwordConfirm');?>"/>
                    <?= $form->error($model, 'passwordConfirm');?>
                <div class="buttons clearfix">
                    <input type="submit" name="login" class="w100" value="Сохранить" />
                </div>
            <? $this->endWidget(); ?>
        </div>

        <div class="notifications">
            <div class="title"><?=Yii::t('secure', 'Notifications');?></div>
            <div class="">
                <?=Yii::t('secure', '');?>
            </div>
            <div class="buttons clearfix">
                <a href="<?=Yii::app()->createUrl('/notification/email');?>"><button class="w100"><?=Yii::t('secure', 'Additional configuration');?></button></a>
            </div>
        </div>
    </div>
</div>

<!-- !!!!!!!!!!!!!!!!!!! -->
<div id="crop-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <b>Сохранение аватара</b>
            </div>
            <form>
                <input type="hidden" id="cropped_image" value=""/>
                <div class="modal-body">
                    <div id="filesInfo" style="width: 600px; margin-bottom: 10px;">
                    </div>
                    <div class="thumbsnail pull-left" style="width: 600px;">
                        <div class="thumbs200 pull-left" style="margin-right: 10px;">
                            <canvas id="t200" width="200" height="200"></canvas>
                        </div>
                        <div class="thumbs100 pull-left" style="margin-right: 10px;">
                            <canvas id="t100" width="100" height="100"></canvas>
                        </div>
                        <div class="thumbs50 pull-left">
                            <canvas id="t50" width="50" height="50"></canvas>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="buttons clearfix">
                    <button class="btn btn-danger" data-dismiss="modal" type="button">Отменить</button>
                    <button class="btn btn-primary pull-left" type="submit">Изменить аватар</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ------------ -->

<? if (Yii::app()->user->hasFlash('notice')) { ?>
    <div id="notice-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-info">
            <div class="modal-content">
                <div class="modal-header">
                    <b><?=Yii::app()->user->getFlash('notice');?></b>
                </div>
            </div>
        </div>
    </div>
<? } ?>

<script type="text/javascript" src="/js/placeholder.js"></script>
