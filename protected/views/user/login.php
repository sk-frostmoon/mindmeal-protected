<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'loginform',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'auth-form',
        'role' => 'form'
    )
)); ?>
    <h1>авторизация mind meal</h1>

    <? if ($url) { ?>
        <input type="hidden" name="url" value="<?=CHtml::encode($url);?>"/>
    <? } ?>

    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
    <?php echo $form->textField($model, 'email',
        array(
            'class' => 'w100',
            'data-focus' => $model->getAttributeLabel('email'),
            'placeholder' => $model->getAttributeLabel('email'),
            'id' => 'login-email',
            'autocomplete' => 'off'
        )
    ); ?>

    <div style="margin-bottom: 15px; width: 100%; text-align: center;" class="input-group">
        <?php echo $form->error($model,'email'); ?>
    </div>

    <? if ($url) { ?>
        <input type="hidden" name="url" value="<?=CHtml::encode($url);?>"/>
    <? } ?>

    <?php echo $form->passwordField($model, 'password',
        array(
            'class' => 'w100',
            'data-focus' => $model->getAttributeLabel('password'),
            'placeholder' => $model->getAttributeLabel('password'),
            'id' => 'login-password',
            'autocomplete' => 'off'
        )
    ); ?>

    <div style="margin-bottom: 0px; text-align: center; display: block;" class="input-group">
        <?php echo $form->error($model,'password'); ?>
    </div>


    <div class="buttons clearfix">
        <button type="button" onclick="window.location.href = '<?=$this->createUrl('/user/register', $url ? array('url'=>$url) : array());?>'; return false;">Зарегистрироваться</button>
        <input type="submit" name="login" value="Войти" />
    </div>
    <div class="bottom clearfix">
        <input type="checkbox" name="remember" /> <label for="remember">Запомнить меня</label>
        <a href="<?=$this->createUrl('/user/forgotten');?>">забыл пароль?</a>
    </div>
<?php $this->endWidget(); ?>

<script type="text/javascript" src="/js/placeholder.js"></script>
