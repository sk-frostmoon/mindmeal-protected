<?
$path = Yii::app()->theme->baseUrl . "/assets";
$cs = Yii::app()->clientScript;
$cs->registerCSSFile($this->createUrl($path . '/css/style_small.css'));

$User = Yii::app()->user->getUserData();

require_once(HESK_PATH . 'inc/popular_question.php');
?>
<div class="heading-bg helpdesk">
    <div class="wrap">
        <h1>Новый запрос</h1>
    </div>
</div>
<? var_export($_SESSION); ?>
<div class="wrap clearfix">
    <div class="main new-ticket">
        <h1> Чтобы получить ответ на интересующий тебя вопрос, заполни указанные ниже формы</h1>

        <form id="ticket-form" name="contact" method="post" action="submit_ticket.php?submit=1" enctype="multipart/form-data">
            <input type="hidden" name="category" value="1" />

            <div class="row">
                <label for="name">ВАШЕ ИМЯ:</label>
                <input type="text" name="name" tabindex="1" id="name" value="<?=$User->username?>" required>
            </div>
            <div class="row">
                <label for="email">ВАШЕ E-MAIL:</label>
                <input type="text" name="email" tabindex="2" id="email" value="<?=$User->email?>" required>
            </div>
            <hr />
            <div class="row">
                <label for="priority">ПРИОРИТЕТНОСТЬ:</label>
                <div class="wrap-select">
                    <select name="priority" id="priority" tabindex="3">
                        <option value="3">НИЗКАЯ</option>
                        <option value="2">СРЕДНЯЯ</option>
                        <option value="1">ВЫСОКАЯ</option>
                    </select>
                </div>
            </div>
            <hr/>
            <div class="row">
                <label for="subject">ТЕМА ЗАПРОСА:</label>
                <input type="text" name="subject" id="subject" tabindex="4" required>
            </div>
            <div class="row">
                <label for="message">ТЕКСТ ЗАПРОСА:</label>
                <textarea id="message" name="message"></textarea>
            </div>
            <hr />
            <div class="row">
                <label>ПРИКРЕПЛЕННЫЕ ФАЙЛЫ:</label>
                <div class="bt-file">
                    <span>Выбрать файл</span>
                    <input type="file" name="attachment[1]" />
                </div>
                <span class="limits">Лимиты загруженых файлов</span>
            </div>
            <hr />
            <div class="row">
                <label for="mysecnum">КАПЧА:</label>
                <input type="text" name="mysecnum" id="mysecnum" />
                <div style="text-align: left; margin-left: 160px; margin-top: 5px;">
                    <img src="print_sec_img.php?39003"
                         alt="Security image" title="Security image" name="secimg" id="secimg">
                    <a href="javascript:void(0);"
                       onclick="document.contact.secimg.src='print_sec_img.php?'+ ( Math.floor((90000)*Math.random()) + 10000);">
                        <img src="support/img/reload.png" height="24" width="24" alt="Reload image" title="Reload image" border="0" style="vertical-align:text-bottom">
                    </a>
                </div>
            </div>
            <hr/>
             <div class="row">
                <input type="submit" value="отправить запрос">
             </div>
        </form>
    </div>
    <div class="right-col">
        <?
        hesk_kbTopArticlesA(8, 0);
        ?>
    </div>
</div>



<!-- !!!!!!!!!!!!!!!!!!! -->
<div id="crop-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <b>Сохранение аватара</b>
            </div>
            <form>
                <input type="hidden" id="cropped_image" value=""/>
                <div class="modal-body">
                    <div id="filesInfo" style="width: 600px; margin-bottom: 10px;">
                    </div>
                    <div class="thumbsnail pull-left" style="width: 600px;">
                        <div class="thumbs200 pull-left" style="margin-right: 10px;">
                            <canvas id="t200" width="200" height="200"></canvas>
                        </div>
                        <div class="thumbs100 pull-left" style="margin-right: 10px;">
                            <canvas id="t100" width="100" height="100"></canvas>
                        </div>
                        <div class="thumbs50 pull-left">
                            <canvas id="t50" width="50" height="50"></canvas>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="buttons clearfix">
                    <button class="btn btn-danger" data-dismiss="modal" type="button">Отменить</button>
                    <button class="btn btn-primary pull-left" type="submit">Изменить аватар</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ------------ -->

<? if (Yii::app()->user->hasFlash('notice')) { ?>
    <div id="notice-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-info">
            <div class="modal-content">
                <div class="modal-header bg-aqua">
                    <b>Notice</b>
                </div>
                <div class="modal-body">
                    <p class="text"><?=Yii::app()->user->getFlash('notice');?></p>
                </div>
            </div>
        </div>
    </div>
<? } ?>
