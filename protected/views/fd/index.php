<?
$path = Yii::app()->theme->baseUrl . "/assets";
$cs = Yii::app()->clientScript;
$cs->registerCSSFile($this->createUrl($path . '/css/style_small.css'));
?>
<div class="heading-bg">
    <div class="wrap">
        <h1>Final Desire</h1>
    </div>
</div>
<div id="profile" class="wrap clearfix">
    <div class="left-col">
        <div class="profile-info">
            <div class="profile-img">
                <form id="uploader" class="profile-avatar col-md-4" style="overflow: hidden;">
                    <img src="<?=$user->getAvatarUrl('230x230');?>" style="max-width: 230px;"/>
                    <input id="avatarUploader" type="file" name="avatar" style="opacity: 0; position: absolute; z-index: 11;" class="btn input-group" onchange="javascript: changeAvatarCrop(this);"/>
                    <a href="#" onclick="return false;">
                        Поменять аватар
                    </a>
                </form>
            </div>
            <div class="profile-nickname">
                <span class="nickname"><?=$user->username;?></span>
                <span class="name"><?=$user->fname;?> <?=$user->lname;?></span>
                <!--<span class="last-network"><span>Был в сети:</span> Вт, 17 сентября 2014г. 22:48</span>-->
            </div>
            <div>
                <h2>Выбери игру</h2>
                <a href="/fd/"  alt="Final Desire"  title="Final Desire"><img style="box-shadow: 0 0 0 2px #e04e39;" src="/themes/mm_secure/assets/img/fd-profile.jpg" alt="Final Desire"/></a>
                <a href="/solgame/"  alt="Shards of Light"  title="Shards of Light"><img src="/themes/mm_secure/assets/img/sol-profile.jpg" alt="Shards of Light"/></a>
            </div>
        </div>
        <div class="main">

        </div>
    </div>
    <div class="right-col">
        <div class="">
            <?
            if($haveAccount) {
                ?>
                <span class="notification">
                    Вы уже приглашены на ЗБТ Final Desire. <br/>
                    Подробности вы можете узнать на сайте игры <a href="http://playfd.ru/site/">Final Desire</a>

                    <span>Ваш Логин в игре: <?= $user->email; ?></span>
                    <span>Пароль совпадает с паролем от учетной записи Mind Meal</span>
                    <? /* <span>Если вы забыли пароль, вы можете его <a href="/solgame/password/">восстановить<a/></span> */?>
                </span>
                <?
            } else {
                ?>
                <span class="notification">
                    Если у Вас есть инвайт-ключ, введите его в форму ниже для участия в ЗБТ Final Desire.
                </span>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'password-change',
                    'method' => 'POST',
                    'enableClientValidation'=>false,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>false,
                    ),
                    'htmlOptions' => array(
                        'class' => 'invite-forum',
                        'role' => 'form'
                    )
                )); ?>

                <div class="title"><?=Yii::t('invite', 'Enter your invite key');?></div>
                <?=$form->textField($model, 'value', array("placeholder" => $model->getAttributeLabel('value')));?>
                <?= $form->error($model, 'value');?>
                <?
                if(!is_null($sg)){
                    echo $form->error($sg, 'sol_register');
                }
                ?>

                <div class="buttons clearfix">
                    <input type="submit" name="login" class="w100" value="Сохранить" />
                </div>
                <?php $this->endWidget(); ?>

                <form id="key-form" class="clearfix" type="POST">
                    <input type="text" name="key" value="" placeholder="НОМЕР КЛЮЧА" data-focus="НОМЕР КЛЮЧА">
                    <input type="submit" name=""  value="Активировать">
                </form>
                <?
            }
            ?>
        </div>
    </div>
</div>

<!-- !!!!!!!!!!!!!!!!!!! -->
<div id="crop-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <b>Сохранение аватара</b>
            </div>
            <form>
                <input type="hidden" id="cropped_image" value=""/>
                <div class="modal-body">
                    <div id="filesInfo" style="width: 600px; margin-bottom: 10px;">
                    </div>
                    <div class="thumbsnail pull-left" style="width: 600px;">
                        <div class="thumbs200 pull-left" style="margin-right: 10px;">
                            <canvas id="t200" width="200" height="200"></canvas>
                        </div>
                        <div class="thumbs100 pull-left" style="margin-right: 10px;">
                            <canvas id="t100" width="100" height="100"></canvas>
                        </div>
                        <div class="thumbs50 pull-left">
                            <canvas id="t50" width="50" height="50"></canvas>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="buttons clearfix">
                    <button class="btn btn-danger" data-dismiss="modal" type="button">Отменить</button>
                    <button class="btn btn-primary pull-left" type="submit">Изменить аватар</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ------------ -->

<? if (Yii::app()->user->hasFlash('notice')) { ?>
    <div id="notice-modal" class="modal fade in" aria-hidden="false" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-info">
            <div class="modal-content">
                <div class="modal-header bg-aqua">
                    <b>Notice</b>
                </div>
                <div class="modal-body">
                    <p class="text"><?=Yii::app()->user->getFlash('notice');?></p>
                </div>
            </div>
        </div>
    </div>
<? } ?>

<script type="text/javascript" src="/js/placeholder.js"></script>
