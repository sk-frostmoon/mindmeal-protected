<?
    /* @var AMQPExtension $this */

    // NOTE: all styles muse be inline!
    //       gmail not supported html5 tags

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <? if (isset($title)) { ?>
        <title><?=$title?></title>
    <? } ?>
</head>
<body data-class="content">
    <div style="display: block;">
        <!-- img src="<?=Yii::app()->createAbsoluteUrl('')?>"/ -->
    </div>
    <div style="display: block;" data-class="container">
        <?=$content;?>
    </div>
    <div style="display: block; margin-top: 20px">
        Best regards, Mind Meal Corp.
    </div>
</body>
</html>
