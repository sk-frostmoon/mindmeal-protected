<?php
$path = Yii::app()->theme->baseUrl . "/assets";
$cs = Yii::app()->clientScript;
$cs->registerCSSFile($this->createUrl($path . '/css/style_small.css'));

$this->pageTitle = 'Расширенная настройка уведомлений';
?>
<style>
.buttons button.w100 { width: 100%; }
.project { padding-left: 25px; }
.project h2 { color: #7c7d80; }
.project.first {
    padding-left: 0px;
}
.project.first h2 {
    color: #e7493a;
}
.wide-col {
    min-height: 820px;
    width: 620px;
    margin: auto;
    padding: 40px;
    background: #f6f7f8;
}
#notification-email .project {
    margin-top: 15px;
}
.project .properties {
    margin-left: 25px;
}
.project .properties .template.disabled {
    color: #888;
}
label[for^=template-]:hover {
    background-color: #eee;
    cursor: pointer;
}
</style>
<div class="heading-bg">
    <div class="wrap">
        <h1><?=Yii::t('secure', 'Notifications')?></h1>
    </div>
</div>
<div id="notification-email" class="wrap clearfix">
    <div class="wide-col">
        <div class="title"><?=Yii::t('secure', 'Notify settings');?></div>
        <? foreach($projects as $pid => $p) { ?>
            <form class="project <?=($pid == 0) ? 'first' : ''?>">
                <div class="name">
                    <h2>
                        <input type="checkbox" id="project-<?=$pid?>" data-project-id="<?=$pid?>" <?=($p['subscribed'] ? 'checked=""' : '')?>/>
                        <?=Yii::t('secure', $p['title']);?>
                    </h2>
                </div>
                <div class="properties">
                    <?
                        foreach($p['templates'] as $tid => $t) {
                            $readonly = false;
                            if (isset($t['htmlOptions'])) {
                                if (isset($t['htmlOptions']['hidden']) && $t['htmlOptions']['hidden']) {
                                    continue;
                                }
                                if (isset($t['htmlOptions']['readonly']) && $t['htmlOptions']['readonly']) {
                                    $readonly = 'readonly="" disabled=""';
                                }
                            }
                    ?>
                        <label for="template-<?=$pid;?>-<?=$tid;?>" class="template <?=($readonly ? 'disabled' : '')?>" style="display: block;">
                            <input type="checkbox" id="template-<?=$pid;?>-<?=$tid;?>" data-template-id="<?=$tid;?>" data-project-id="<?=$pid;?>" <?=$readonly;?> <?=($t['subscribed'] ? 'checked=""' : '')?>/>
                            <?=$t['subject']?>
                        </label>
                    <?}?>
                </div>
            </form>
        <? } ?>

        <div class="buttons clearfix">
            <a href="<?=Yii::app()->createUrl('/user/edit');?>"><button class="w100"><?=Yii::t('secure', 'Back');?></button></a>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#notification-email input[data-project-id]').each(function (k,o) {
        $(o).click(function () {
            if ($(o).prop('disabled') || $(o).prop('readonly')) {
                return;
            }
            var data = {
                project_id: $(o).attr('data-project-id'),
                value: $(o).prop('checked') ? '1' : '0',
            };
            if ($(o).attr('data-template-id')) {
                data['template_id'] = $(o).attr('data-template-id');
            }

            $.ajax({
                url: '<?=$this->createUrl('/notification/emailSubscribeAjax')?>',
                method: 'POST',
                data: data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                    } else {
                        $(o).prop('checked', !$(o).prop('checked'));
                    }
                },
                error: function (response) {
                    $(o).prop('checked', !$(o).prop('checked'));
                }
            });
        });
    });
</script>
