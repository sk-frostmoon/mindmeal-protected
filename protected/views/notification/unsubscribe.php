<?php
$path = Yii::app()->theme->baseUrl . "/assets";
$cs = Yii::app()->clientScript;
$cs->registerCSSFile($this->createUrl($path . '/css/style_small.css'));

$this->pageTitle = 'Отписка от рассылки';
?>
<style>
.w100 { width: 100%; }
.wide-col {
    min-height: 820px;
    width: 620px;
    margin: auto;
    padding: 40px;
    background: #f6f7f8;
}
.info {
    font-size: 16px;
    margin-top: 25px;
}
.buttons { margin-top: 20px; }
.buttons button.w100 { width: 100%; }

</style>
<div class="heading-bg">
    <div class="wrap">
        <h1>Отказ от рассылки</div>
    </div>
</div>
<div id="notification-email" class="wrap clearfix">
    <div class="wide-col">
        <? $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'unsubscribe-form',
                    'action' => '',
                    'method' => 'POST',
        )); ?>
            <h2><?=Yii::t('secure', 'Dear User');?></h2>
            <p class="info">
                На данной странице вы можете отписаться от рассылки отправляемой со всех проектов нашей кампании за один клик.<br/>
            </p>
            <p class="info">
                Если вы хотите отписаться только от какого-то письма из рассылки или проекта в целом, вы можете сделать это <a href="<?=Yii::app()->createUrl('/notification/email');?>">здесь</a>
            </p>
            <p class="info">
                Так же вы всегда сможете восстановить подписку на странице "<a class="settings" href="<?=Yii::app()->createUrl('/user/edit')?>">Настройки</a>" в разделе "Рассылка"
            </p>
            <div class="buttons clearfix">
                <input type="hidden" name="confirm" value="1"/>
                <button class="w100" type="submit"><?=Yii::t('secure', 'Отписаться');?></button>
            </div>
        <? $this->endWidget(); ?>
    </div>
</div>

