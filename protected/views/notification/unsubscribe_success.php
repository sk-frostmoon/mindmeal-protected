<?php
$path = Yii::app()->theme->baseUrl . "/assets";
$cs = Yii::app()->clientScript;
$cs->registerCSSFile($this->createUrl($path . '/css/style_small.css'));

$this->pageTitle = 'Отписка от рассылки';
?>
<style>
.w100 { width: 100%; }
.wide-col {
    min-height: 820px;
    width: 620px;
    margin: auto;
    padding: 40px;
    background: #f6f7f8;
}
.info {
    font-size: 18px;
    margin-top: 25px;
}
.buttons { margin-top: 25px; }
.buttons button.w100 { width: 100%; }
</style>
<div class="heading-bg">
    <div class="wrap">
        <h1>Отписка от рассылки</div>
    </div>
</div>
<div id="notification-email" class="wrap clearfix">
    <div class="wide-col">
        <p class="info">
            <?=Yii::t('secure', 'Вы были успешно отписаны от рассылки.');?>
        </p>
        <p class="info">
            <?=Yii::t('secure', 'Теперь вы не будете получать уведомления даже о новых проектах кампании.');?>
        </p>
        <p class="info">
            <?=Yii::t('secure', 'Спасибо, что следили за нашими новостями.');?>
        </p>
        <div class="buttons clearfix">
        <? if (Yii::app()->user->getIsGuest()) { ?>
            <a href="<?=Yii::app()->crweateAbsoluteUrl('/');?>"><button class="w100"><?=Yii::t('secure', 'На страницу входа');?></button></a>
        <? } else { ?>
            <a href="<?=Yii::app()->createUrl('/user/profile');?>"><button class="w100"><?=Yii::t('secure', 'Вернуться к профилю');?></button></a>
        <? } ?>
        </button>
    </div>
</div>
