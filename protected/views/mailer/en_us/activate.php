<? /*
    * @var AMQPExtension $this
    * @var WebUser $this->currentUser
    */ ?>
<h1>Thank you for join to us!</h1>
<div>
    You received this message becose somebody use this email address in <a href="http://secure.mindmeal.ru">Mind Meal Corp</a> registration form.<br/>
    <br/>
    To finish registration follow to the next link:</br>
    <a href="<?=Yii::app()->createAbsoluteUrl('/user/activate', array('token' => $token));?>"><?= CHtml::encode( Yii::app()->createAbsoluteUrl('/user/activate', array('token' => $token)) );?></a>
</div>
