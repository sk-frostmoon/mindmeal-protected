<? /*
    * @var AMQPExtension $this
    * @var WebUser $this->currentUser
    */ ?>
<h1>Hi!</h1>
You've received this mail becose someone try to restore your account password,<br/>
if it isn't you just ignore this mail.<br/>
<br/>
To continue restoring password follow to link:<br/>
<a href="<?=CHtml::encode( Yii::app()->createAbsoluteUrl('/user/restore', array('token' => $user->getToken())) );?>"><?=CHtml::encode( Yii::app()->createAbsoluteUrl('/user/restore', array('token' => $user->getToken())) );?></a>
