<? /*
    * @var AMQPExtension $this
    * @var WebUser $this->currentUser
    */ ?>
<h1>Спасибо, что пришли к нам!</h1>
<div>
    Вы получили это сообщение потому, что кто-то указал Ваш E-Mail адрес в форме регистрации на сайте <a href="http://secure.mindmeal.ru">Mind Meal Corp</a>.<br/>
    <br/>
    Для завершения регистрации перейдите по следующей ссылке:<br/>
    <a href="<?=Yii::app()->createAbsoluteUrl('/user/activate', array('token' => $token));?>"><?= CHtml::encode( Yii::app()->createAbsoluteUrl('/user/activate', array('token' => $token)) );?></a>
</div>
