<? /*
    * @var AMQPExtension $this
    * @var WebUser $this->currentUser
    */ ?>
<h1>Здравствуйте!</h1>
Вы получили это письмо потому, что кто-то перешел по ссылке восстановления пароля на сайте <a href="http://secure.mindmeal.ru">Mind Meal Corp</a>,<br/>
если это были не Вы, просто игнорируйте это письмо</br/>
<br/>
Для продолжения процедуры восстановления пароля перейдите по следующей ссылке:<br/>
<a href="<?=CHtml::encode( Yii::app()->createAbsoluteUrl('/user/restore', array('token' => $user->getToken())) );?>"><?=CHtml::encode( Yii::app()->createAbsoluteUrl('/user/restore', array('token' => $user->getToken())) );?></a>
