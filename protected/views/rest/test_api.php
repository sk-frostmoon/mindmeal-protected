<? /* @var Controller $this */ 
$cs = Yii::app()->clientScript;
$cs->registerScriptFile(Yii::app()->baseUrl . '/js/base64.js');
$cs->registerScriptFile(Yii::app()->baseUrl . '/js/sha1.js');
$cs->registerScriptFile(Yii::app()->baseUrl . '/js/api.js');
?>
<style>
.login-bar {
    width: 15px;
    height: 15px;
    background: #F00;
    border-radius: 50%;
    float: left;
    margin-right: 5px;
}
.login-bar.online {
    background: #0F0;
}
@keyframes kfBreathe {
    0%, 100% {
        transform: scale(1.0, 1.0);
    }
    50% {
        transform: scale(1.5, 1.5);
    }
}
.breathe {
    transition: kfBreathe 2s forwards;
}
</style>
<script type="text/javascript">
function logMessage(text) {
    $('#log').append($('<div class="display: block;">' + text + '</div>'));
}

var timer;
var aliveTiming = 30000; // 30s
function aliveCall() {
    mmApi.query('<?=Yii::app()->createUrl('/api/user/alive');?>', {app:'finaldesire'}, cbAlive);
}
function toggleAlive(btn) {
    if (timer) {
        window.clearInterval(timer);
        timer = null;
        btn.innerHTML = 'Start alive ping';
    } else {
        btn.innerHTML = 'Stop alive ping';
        timer = window.setInterval(aliveCall, aliveTiming);
        aliveCall();
    }
}

var testing     = false;
var testingStep = 0;

function testingAll() {
    if (testingStep == 0) { $('#loginBtn').click(); }
    if (testingStep == 1) { $('#aliveBtn').click(); }
    if (testingStep == 2) { $('#getProfileBtn').click(); }
    if (testingStep == 3) { $('#getPublicInfoBtn').click(); }
    if (testingStep == 4) { $('#byebyeBtn').click(); }

    testingStep++;
}

function cbLogin (status, data) {
    var s = ((status == 'success') ? 'success' : 'error');
    //console.log(status, data);
    $('#launcher-status').html('runned');
    logMessage('Login ' + s + '. ' + ((status == 'success') ? '' : data.error.type + ' => ' + data.error.message));
    if (status == 'success') {
        $('.login-bar').addClass('online');
        if ((!testing) && (!timer)) $('#toggleAliveBtn').click();
    }
    if (testing) { testingAll(); }
}
function cbAlive(status, data) {
    if (status == 'success') {
        //logMessage('Alive query ' + data.alive);
        $('.login-bar').addClass('online');
        $('.login-bar').addClass('breathe');
        window.setTimeout(function () { $('.login-bar').removeClass('breathe'); }, 2000);
        if (testing) { testingAll(); }
    }
    else {
        $('.login-bar').removeClass('online');
        if (data.error) {
            logMessage('Alive query failed. ' + data.error.type + ' => ' + data.error.message);
        } else {
            logMessage('Successful execution: Other application cannot be setted.');
        }
    } 
}
function cbGetProfile(status, data) {
    if(status == 'success') {
        logMessage('Profile query id:' + data.profile.id + '; username:' + data.profile.username);
        if (testing) { testingAll(); }
    }
    else { logMessage('Alive query failed. ' + data.error.type + ' => ' + data.error.message); } 
}
function cbGetPublicInfo(status, data) {
    if(status == 'success') {
        logMessage('GetPublicInfo query: ' + data.users.length);
        for(var i=0; i<data.users.length; i++) {
            var u = data.users[i];
            logMessage('---- User[' + u.id + '] => [' + u.state + '] ' + u.level + 'LVL; '+ u.avatar);
        }
        if (testing) { testingAll(); }
    }
    else { logMessage('Alive query failed. ' + data.error.type + ' => ' + data.error.message); } 
}
function cbByebye(status, data) {
    $('.login-bar').removeClass('online');
    $('#launcher-status').html('closed');
    if (status == 'success') {
        logMessage('Byebye!');
        if (timer) { $('#toggleAliveBtn').click(); }
        if (testing) { testingAll(); }
    }
    else { logMessage('Logout query failed. ' + data.error.type + ' => ' + data.error.message); } 
}

function cbClientReady(status, data) {
    console.log(status, data);
    var cs = $('#client-status');
    if (status == 'success') {
        cs.html('runned');
        cs.data().status = 'runned';
        aliveTiming = 15000; // 15s
        $('#toggleAliveBtn').click().click(); // Refresh for a faster timings
    } else {
        cs.html('run failed');
        window.setTimeout(function () {cs.html(cs.data().status)}, 3500);
    }
}

function cbClientPlay(status, data) {
    console.log(status, data);
    var cs = $('#client-status');
    if (status == 'success') {
        cs.html('waiting queue');
        cs.data().status = 'waiting queue';
    } else {
        cs.html('play failed');
        window.setTimeout(function () { cs.html(cs.data().status)}, 3500);
    }
}
function cbClientClosed(status, data) {
    console.log(status, data);
    var cs = $('#client-status');
    if (status == 'success') {
        cs.html('closed');
        cs.data().status = 'closed';
        aliveTiming = 30000; // 30s
        $('#toggleAliveBtn').click().click(); // Refresh for a faster timings
    } else {
        cs.html('closing failed');
        window.setTimeout(function () { cs.html(cs.data().status)}, 3500);
    }
}

function cbServerInGame(status, data) {
    console.log(status, data);
    var cs = $('#client-status');
    if (status == 'success') {
        cs.html('in_game');
        cs.data().status = 'in_game';
    } else {
        cs.html('broken join to game');
        window.setTimeout(function () { cs.html(cs.data().status)}, 3500);
    }
}


function cbGetState(status, data) {
    console.log(status, data);
    if (status == 'success') {
        logMessage('State: ' + data.getState);
    }
}
function cbGetStatus(status, data) {
    console.log(status, data);
    if (status == 'success') {
        logMessage('Status: ' + data.getStatus);
    }
}
function cbRunGame(status, data) {
    console.log(status, data);
}
function cbPlayGame(status, data) {
    console.log(status, data);
}
function cbSessionReady(status, data) {
    console.log(status, data);
}
function cbEndGame(status, data) {
    console.log(status, data);

}


</script>
<div id="log" style="height: 250px; width: 100%; border: 1px solid #888; border-radius: 3px; margin-bottom: 10px; overflow: auto;">
</div>
<div style="display: block;">
    <div class="login-bar"></div> <em>Launcher: <span id="launcher-status">runned</span></em> <em>Client: <span id="client-status">closed</span></em>
    <br/>
    
    <label>Login to api</label>
    <input id="auth_user" type="text" name="email" value=""/>
    <input id="auth_pw"   type="password" name="password" value=""/>
    <button id="loginBtn" type="button" onclick="javascript: mmApi.login($('#auth_user').val(), $('#auth_pw').val(), cbLogin); return false;">Login</button>
    <div style="float: right;">
        <button id="testingAllBtn" type="button" onclick="javascript: testing = true; testingStep = 0; testingAll();">TestingAll</button>
        <button id="clearLogBtn"   type="button" onclick="javascript: $('#log').html('');">Clear Log</button>
    </div>
    <br/>
    
    <label>Login by tk</label>
    <input id="auth_token" type="text" name="token" value=""/>
    <button id="tokenLoginBtn" type="button" onclick="javascipt: mmApi.token = $('#auth_token').val(); mmApi.email = $('#auth_user').val(); $('#toggleAliveBtn').click();">Token Login</button>
</div>
<div style="display: block;">
    <label for="toogleWhenLogin"><input type="checkbox" checked> Start ping when logged in</label>
</div>

<button id="toggleAliveBtn" type="button" onclick="javascript: toggleAlive(this);">Start alive ping</button>
<button id="aliveBtn" type="button" onclick="javascript: aliveCall();">Alive</button>
<button id="aliveOtherBtn" type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/alive');?>', {app:'nextcoolgame'}, cbAlive);">Wrong Alive</button>
<button id="getProfileBtn" type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/getProfile');?>', {}, cbGetProfile);">Get Profile</button>
<button id="getPublicInfoBtn" type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/getPublicInfo');?>', {id:[19]}, cbGetPublicInfo);">Get Public Info</button>

<button id="byebyeBtn" type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/logout');?>', {}, cbByebye);">Logout</button>

<br/>
<button id="clientReadyBtn" type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/clientReady');?>', {}, cbClientReady);">Client Ready</button>
<button id="clientPlayBtn"  type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/clientPlay');?>', {}, cbClientPlay);">Client Play</button>
<button id="clientCloseBtn" type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/clientClosed');?>', {}, cbClientClosed);">Client Close</button>
<button id="serverInGameBtn" type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/setStatus');?>', {status:'in_game'}, cbServerInGame);">Server Told In Game</button>

<br/>
<br/>

Set States:<br/>
<button id="getState" type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/getState');?>', {}, cbGetState);">Get State</button>

<br/><br/>
Set Statuses:<br/>
<button id="getStatus"    type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/getStatus');?>', {}, cbGetStatus);">Get Status</button>
<button id="runGame"      type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/setStatus');?>', {status:'lobby'}, cbRunGame);">Set Lobby</button>
<button id="playGame"     type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/setStatus');?>', {status:'waiting_queue'}, cbPlayGame);">Set Waiting Queue</button>
<button id="sessionReady" type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/setStatus');?>', {status:'in_game'}, cbSessionReady);">Set In Game</button>
<!-- <button id="endGame"      type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/setStatus');?>', {status:'lobby'}, cbEndGame);"></button -->
<!-- button id="runGame"     type="button" onclick="javascript: mmApi.query('<?=Yii::app()->createUrl('/api/user/setStatus');?>', {status:'lobby'}, cbRunGame);">Run</button -->


<div style="display: block; height: 150px;">
</div>

<!-- form action="" method="POST" enctype="multipart/form-data" id="MyUploadForm">
    <input type="submit" value="upload"/>
    <input name="ImageFile" id="imageInput" type="file" multiple="1"/>
</form>

<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/jquery.form.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    var options = {
            target:   '#output',   // target element(s) to be updated with server response
            beforeSubmit:  beforeSubmit,  // pre-submit callback
            resetForm: true        // reset the form after successful submit
        };
       
     $('#MyUploadForm').submit(function() {
            $(this).ajaxSubmit(options);  //Ajax Submit form           
            // return false to prevent standard browser submit and page navigation
            return false;
        });
});</script>

<script type="text/javascript">
        //function to check file size before uploading.
function beforeSubmit(){
    //check whether browser fully supports all File API
   if (window.File && window.FileReader && window.FileList && window.Blob)
    {
       
        if( !$('#imageInput').val()) //check empty input filed
        {
            $("#output").html("Are you kidding me?");
            return false;
        }
       
        var fsize = $('#imageInput')[0].files[0].size; //get file size
        var ftype = $('#imageInput')[0].files[0].type; // get file type

        console.log($('#imageInput')[0].files);

        return false;
       

        //allow only valid image file types
        switch(ftype)
        {
            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                break;
            default:
                $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
                return false
        }
       
        //Allowed file size is less than 1 MB (1048576)
        if(fsize>1048576)
        {
            $("#output").html("<b>"+fsize +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
            return false
        }
               
        $('#submit-btn').hide(); //hide submit button
        $('#loading-img').show(); //hide submit button
        $("#output").html("");  
    }
    else
    {
        //Output error to older unsupported browsers that doesn't support HTML5 File API
        $("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
        return false;
    }
}
</script -->
<!-- div style="display: block">
    <select id="">
    </select 
</div -->
