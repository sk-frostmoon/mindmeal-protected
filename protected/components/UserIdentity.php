<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    protected $_id;
    protected $_email;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */

    public $_userData = array();

    public $errors = array();

	/**
	 * Constructor.
	 * @param string $username username
	 * @param string $password password
	 */
	public function __construct($email,$password)
	{
		$this->_email=$email;
		$this->password=$password;
	}

    public function getId()
    {
        return $this->_id;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setEmail($email)
    {
        $this->_email = $email;
    }

    public function authenticate()
    {
        // @ak: Authenticate checked in UserModel::validCredentials 
        Yii::log($this->_email . ':' . sha1($this->password), 'trace');
        $user = UserModel::model()->find("LOWER(email) = :email AND password = :password",
            array(
                'email'     => strtolower($this->_email),
                'password'  => sha1($this->password),
        ));

        if (is_null($user)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            if ($user->getIsActivated()) {
                $this->_id    = $user->id;
                $this->_email = $user->email;

                Yii::app()->user->setCustom($user);
                $this->errorCode = self::ERROR_NONE;
            } else {
                $this->errors[] = Yii::t('yii', 'Your account will not be activated');
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
        }

        return !$this->errorCode;
    }
}
