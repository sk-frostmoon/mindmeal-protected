<?php

class RestWebUser extends WebUser
{
    protected $_email;
    protected $_token;
    protected $_resttoken;
    protected $_statesSession;

    protected $_userData;

    // public setState(key,value,defaultValue=null) {{{ 
    /**
     * setState
     * 
     * @param mixed $key 
     * @param mixed $value 
     * @param mixed $defaultValue 
     * @access public
     * @return void
     */
    public function setState($key, $value, $defaultValue=null)
    {
        $key=$this->getStateKeyPrefix().$key;
        if($value===$defaultValue)
            unset($this->_statesSession[$key]);
        else
            $this->_statesSession[$key]=$value;
    }
    // }}}

    // public getState(key,defaultValue=null) {{{ 
    /**
     * getState
     * 
     * @param mixed $key 
     * @param mixed $defaultValue 
     * @access public
     * @return void
     */
    public function getState($key, $defaultValue=null)
    {
        $key=$this->getStateKeyPrefix().$key;
        return isset($this->_statesSession[$key]) ? $this->_statesSession[$key] : $defaultValue;
    }
    // }}}

    /**
     * Метод хранящий пользовательские данные о клиенте
     * @param $data
     */
    public function setCustom($data) {
        // Nothing to do, just stop session saving
        //Yii::app()->session['restUserData'] = $data;
        $this->_userData = $data;
    }

    // public getRestfullToken() {{{ 
    /**
     * getRestfullToken
     * 
     * @access public
     * @return RestfullToken|NULL
     */
    public function getRestfullToken()
    {
        if ($this->_resttoken == NULL)
        {
            $this->_resttoken = RestfullToken::findByEmail($this->_email);
        }
        return $this->_resttoken;
    }
    // }}}

    private function setRestfullToken(RestfullToken $token)
    {
        $this->_resttoken = $token;
    }


    // public getToken() {{{ 
    /**
     * getToken
     * 
     * @access public
     * @return string|NULL
     */
    public function getToken()
    {
        if ($this->_token == NULL) {
            $tk = $this->getRestfullToken();
            if ($tk !== false) {
                $this->_token = $tk->token;
            }
        }
        return $this->_token;
    }
    // }}}

    /**
     * Метод хранящий пользовательские данные о клиенте
     */
    public function getUserData() {
        if (!$this->_userData) {
            $token = $this->getRestfullToken();
            if ($token) {
                $this->_userData = $token->user;
            }
        }
        return $this->_userData;
    }

    public function loadIdentityStates($states)
    {
        return array();
    }

    public function changeIdentity($id,$name,$states)
    {
        // Regenerate session only when logout on secure
        $userData = Yii::app()->user->getUserData();

        $username = NULL;
        if (!empty($userData)) {
            if (!empty($userData['username'])) {
                $username = $userData['username'];
            } else if ((!empty($userData['fname'])) || (!empty($userData['lname']))) {
                $username = $userData['fname'] . ' ' . $userData['lname'];
            } else {
                $username = $userData['email'];
            }
        }

        $this->setId($userData->id);
        $this->setName($username);
        $this->setEmail($userData->email);
        $this->loadIdentityStates($states);
    }

    private function saveRestfullToken($tk, $time, $user)
    {
        $token = new RestfullToken();
        $token->token       = $tk;
        $token->joinedTime  = $time;
        $token->user        = $user;
        if (strcmp("sk@mindmeal.ru", $user->email) === 0) {
            // @ak Debug user must be already in online to call isOnline api
            $token->state  = RestfullToken::STATE_ONLINE;
            $token->status = RestfullToken::STATUS_LOBBY;
        }
        $token->save();
        $this->setRestfullToken($token);
        return $token;
    }

    public function loginByWSSE($authWSSE)
    {
        if (isset($authWSSE['Username'])
            && isset($authWSSE['PasswordDigest'])
            && isset($authWSSE['Nonce'])
            && isset($authWSSE['Created'])
        ) {
            $currTime = strtotime(date('r'));
            $userTime = (int)$authWSSE['Created'];
            
            if ( (max($currTime, $userTime) - min($currTime, $userTime)) > 60)
            {
                throw new \CHttpException(401, 'Request packet too old SERVTIME[' . $currTime . '] USERTIME[' . $userTime . ']');
            }

            $this->_email = $authWSSE['Username'];
            $token = $this->getRestfullToken();
            if ($token === false) {
                $user = RestUserModel::model()->findByAttributes(array('email'=>$authWSSE['Username']));
                if ($user === NULL) {
                    throw new \CHttpException(401, 'Udefined auth user. Reason: token not found');
                }
                $token = $this->saveRestfullToken($user->getToken(), time(), $user);
            }

            // Nonce already b64 encoded
            $digest = base64_encode(sha1($authWSSE['Nonce'] . $userTime . $token->token));
            //$digest = base64_encode(sha1(base64_encode($authWSSE['Nonce']) . $authWSSE['Created'] . $token->token, true));
            
            if (strcmp($digest, $authWSSE['PasswordDigest']) === 0) {
                $this->_token = $token->token;
                $id=$token->user->username;
                $states=array();
                if($this->beforeLogin($id,$states,false))
                {
                    $this->changeIdentity($id,$id,$states);
                }
                return !$this->getIsGuest();
            }
        }
        throw new CHttpException(401, 'Token key is invalid');
    }

    public function loginToWSSE($userIdentity)
    {
        $id=$userIdentity->getId();

        $this->_email = $userIdentity->getEmail();
        $states = $userIdentity->getPersistentStates();
        if ($this->beforeLogin($id,$states,false))
        {
            $token  = $this->getRestfullToken();
            if ($token === false) {
                $user  = RestUserModel::model()->findByAttributes(array('email'=>$this->_email));
                $token = $this->saveRestfullToken($userIdentity->getToken(), time(), $user);
            }
    
            $this->_token = $token->token;

            $this->changeIdentity($id,$userIdentity->getName(),$states);
        }
        return !$this->getIsGuest();
    }

    public function logout()
    {
        $this->getRestfullToken()->endApplication();
        $this->getRestfullToken()->byebye();
        return true;
    }

    // public refreshAliveTime() {{{ 
    /**
     * refreshAliveTime
     * 
     * @access public
     * @return void
     */
    public function refreshAliveTime()
    {
        $token = $this->getRestfullToken();
        if ($token) {
            $token->touchAlive();
        }
    }
    // }}}
}
