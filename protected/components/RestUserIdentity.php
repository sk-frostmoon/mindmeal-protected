<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class RestUserIdentity extends UserIdentity
{
    protected $_token;

    public function getToken()
    {
        return $this->_token;
    }

    public function setToken($tk)
    {
        $this->_token = $tk;
    }
}
