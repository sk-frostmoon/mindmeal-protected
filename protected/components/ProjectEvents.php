<?
class ProjectEvents
{
    static public function onUserLogin($event)
    {
        Yii::trace(__METHOD__ . ' called');
    }

    static public function onUserRegister($event)
    {
        $user = isset($event->params['user']) ? $event->params['user'] : UserModel::model()->findByPk(Yii::app()->user->id);
        Yii::trace(__METHOD__ . ' called');
        Yii::app()->mailer->postRenderPublish($user->email, SecureProject::TPL_REGISTER_GREETINGS);

        if(strpos($event->params['host'], "solgame.ru") !== false){
        } elseif (strpos($event->params['host'], "finaldesire.ru") !== false || strpos($event->params['host'], "playfd.ru") !== false){
            Yii::app()->mailer->postRenderPublish($user->email, array('FinalDesireProject', FinalDesireProject::TPL_ZBT_OPENING) );
        }

        if (defined('IS_REST_REQUEST') && IS_REST_REQUEST) {
            //$this->addStatisticCookie('stat_push', 'user_register', array('method' => 'api'));
        }
        //@sk
        //@sk need check isset solgame account
        $gameAccountsModel = new GameAccountsModel();
        $haveAccount = $gameAccountsModel->checkAccountByName("solgame", $user->id);

        if (!$haveAccount) {
             $result = $gameAccountsModel->createAccount("solgame", $user->id);
        }
    }

    static public function sendMailSolAccountCreated($event)
    {
        $user = $event->params['user'];
        Yii::app()->mailer->postRenderPublish($user->email, array('SecureProject', SecureProject::TPL_SOLGAME_INVITE_ACTIVATE), array(
            'userLogin' => $user->username,
        ));
    }

    static public function createSolAccount($event)
    {
        $user = isset($event->params['user']) ? $event->params['user'] : UserModel::model()->findByPk(Yii::app()->user->id);

        //@sk
        try {
            Yii::app()->sol_game_db;
            $sg = new SolGame('insert');
            $sg->onAccountCreated = array('ProjectEvents', 'sendMailSolAccountCreated');

            if ($sg->createAccount($user->id)) {
                //@sk
                Yii::log("Welcome New User -> " . $user->email, 'error');
                //Yii::app()->mailer->postRenderPublish($user->email, array('SecureProject', 5), array(
                //        'userLogin' => $user->username,
                //));
            } else {
                $errors = $this->getErrors();
                if (isset($errors['sol_account_exists'])) {
                    Yii::log("Welcome New User Twice  -> " . $user->email, 'error');
                } else {
                    Yii::app()->mailer->adminAlert(array(
                                'template'      => 'SecureProject::TPL_SOLGAME_INVITE_ACTIVATE',
                                'user'          => CVarDumper::dumpAsString($user),
                                'modelErrors'   => CVarDumper::dumpAsString($sg->getErrors()) . " !!!!!!",
                    ));
                    Yii::log('Ошибка при создании аккаунта SOL: ' . serialize($sg->getErrors()), 'error');
                }
            }

            return !$sg->hasErrors();

        } catch(CDbException $e) {
            Yii::app()->mailer->adminAlert(array(
                        'template'      => 'SecureProject::TPL_SOLGAME_INVITE_ACTIVATE',
                        'user'          => CVarDumper::dumpAsString($user),
                        'exception'     => $e->getMessage(),
                        'modelErrors'   => ($sg instanceof SolGame) ? CVarDumper::dumpAsString($sg->getErrors()) : null,
            ));
            Yii::log(__METHOD__ . ' exception catched: ' . $e->getMessage(), 'error');

            return false;
        }
    }
}
