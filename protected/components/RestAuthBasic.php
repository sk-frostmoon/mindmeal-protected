<?php

class RestAuthBasic extends \rest\service\auth\adapters\Basic
{
    public $identityClass = 'application.components.RestUserIdentity';

    protected function parseXWSSE($rawHeader)
    {
        $aRaw = preg_split('/,? /', $rawHeader);
        $rawHeader = array();
        foreach($aRaw as $item)
        {
            try {
                if (strpos($item, '=') !== false) {
                    list($key, $val) = explode('=', $item, 2);
                    $rawHeader[$key] = preg_replace('/(^"|"$)/u', '', $val);
                } else {
                    $rawHeader[$item] = true;
                }
            } catch(\Exception $e) {
                unset($e);
            }
        }

        return $rawHeader;
    }

    protected function canActionWithoutAuth()
    {
        $rest       = \Yii::app()->createController('rest');
        $requestUri = \Yii::app()->request->requestUri;
        foreach ($rest[0]->accessRules() as $rule) {
            if (
                isset($rule[0]) && ($rule[0] == 'allow') 
                && isset($rule['users']) && (!empty($rule['users'])) && ($rule['users'][0] == '*')
            )
            {

                foreach ($rule['actions'] as $action) {
                    if (strcmp($requestUri, '/api/user/' . $action) === 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function authenticate()
    {
        // Generated from auth method
        /*
        $token = sha1("".(time()*rand()*1.2387654));

        $username = 'ak@mindmeal.ru';
        $nonce    = hash('sha512', uniqid(true));
        $created  = strtotime(date('r'));

        $digest   = base64_encode(sha1(base64_encode($nonce).$created.$token, true));
        $authWSSE = 'UsernameToken Username="' . $username . '", PasswordDigest="' . $digest . '", Nonce="' . $nonce . '", Created="' . $created . '"';
        */

        // This string we get from user on every request
        $authWSSE = isset($_SERVER['HTTP_X_WSSE']) ? $_SERVER['HTTP_X_WSSE'] : false;
        if ($authWSSE != false) {
            $authWSSE = $this->parseXWSSE($authWSSE);
            if (isset($authWSSE) && $authWSSE['UsernameToken']) {
                \Yii::app()->user->loginByWSSE($authWSSE);
            } else {
                throw new \CHttpException(400, 'Unknown WSSE request');
            }

            // If request is valid
            return;
        }

        // Recieve auth_user and auth_pw only on login
        if (strcmp(Yii::app()->request->requestUri, '/api/user/login') !== 0) {
            if ($this->canActionWithoutAuth()) {
                return;
            }

            // Basic authorization only on /api/user/login
            throw new \CHttpException(401, \Yii::t('ext', 'Undefined auth user. Reason: authorize first.'));
        }
        
        $user     = \Yii::app()->request->getParam('auth_user', false);
        $password = \Yii::app()->request->getParam('auth_pw', false);

        if (!$user || !$password) {
            throw new \CHttpException(401, \Yii::t('ext', 'Undefined auth user. Reason: invalid login or password'));
        }

        $identityClass = \Yii::import($this->identityClass);
        $identity = new $identityClass($user, $password);
        if (!$identity->authenticate()) {
            $message = empty($identity->errorMessage) ? 'Undefined auth user. Reason: invalid login or password' : $identity->errorMessage;
            throw new \CHttpException(401, $message);
        }

        // IP
        // DOMAIN
        // TIME_LOGIN

        try {
            $identity->setToken( Yii::app()->user->getUserData()->getToken() );
        } catch(Exception $e) {
            throw new \CHttpException(404, "Undefined auth user. Reason: something gone wrong. Try again later");
        }
        \Yii::app()->user->loginToWSSE($identity);
    }
}
