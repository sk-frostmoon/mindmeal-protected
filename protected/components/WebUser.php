<?php
class WebUser extends CWebUser
{
    protected $_email;

    public function getEmail()
    {
        if (!isset($this->_email)) {
            $data = $this->getUserData();
            $this->_email = isset($data['email']) ? $data['email'] : null;
        }
        return $this->_email;
    }

    public function setEmail($email)
    {
        $this->_email = $email;
    }

    /**
     * Метод хранящий пользовательские данные о клиенте
     * @param $data
     */
    public function setCustom($data) {
        Yii::app()->session['userData'] = serialize($data);
    }

    /**
     * Метод хранящий пользовательские данные о клиенте
     */
    public function getUserData() {
        return isset(Yii::app()->session['userData']) ? unserialize(Yii::app()->session['userData']) : null;
    }

    // public getFirstLogin() {{{ 
    /**
     * getFirstLogin
     * 
     * @access public
     * @return boolean|array
     */
    public function getFirstLogin() {
        return isset(Yii::app()->session['__user:firstLogin']) ? Yii::app()->session['__user:firstLogin'] : false;
    }
    // }}}

    public function changeIdentity($id,$name,$states)
    {
        // Regenerate session only when logout on secure
        $userData = $this->getUserData();

        $username = NULL;
        if (!empty($userData)) {
            if (!empty($userData->username)) {
                $username = $userData->username;
            } else if ((!empty($userData->fname)) || (!empty($userData->lname))) {
                $username = trim($userData->fname . ' ' . $userData->lname);
            } else {
                $username = $userData->email;
            }
        }

        $this->setId($userData->id);
        $this->setName($username);
        $this->setEmail($userData->email);
        $this->loadIdentityStates($states);
    }

    public function login($identity, $duration=0)
    {
        parent::login($identity, $duration);
        if ($this->hasEventHandler('onUserLogin')) {
            $this->onUserLogin(new CEvent($this, array('email' => $identity->email)));
        }
    }

    public function onUserLogin($event)
    {
        $this->raiseEvent('onUserLogin', $event);
    }

    public function onUserRegister($event)
    {
        $this->raiseEvent('onUserRegister', $event);
    }

    public function onUserPasswordChanged($event)
    {
        $this->raiseEvent('onUserPasswordChanged', $event);
    }
}
