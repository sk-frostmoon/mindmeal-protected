<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public $statusCode;
    public $message;

    public function behaviors()
    {
        return array(
                'restAPI' => array('class' => '\rest\controller\Behavior'),
        );
    }

    // public render(view,data=null,return=false,arrayfields=array()) {{{ 
    /**
     * render
     * Rest render
     * 
     * @param mixed $view 
     * @param mixed $data 
     * @param mixed $return 
     * @param array $fields 
     * @access public
     * @return void
     */
    public function render($view, $data = null, $return = false, array $fields = array())
    {
        if (($behavior = $this->asa('restAPI')) && $behavior->getEnabled()) {
            return $this->renderRest($view, $data, $return, $fields);
        } else {
            if ($this->hasEventHandler('onBeforeRender')) {
                $this->onBeforeRender(new CEvent($this));
            }
            $result = parent::render($view, $data, $return);
            if ($this->hasEventHandler('onAfterRender')) {
                $this->onAfterRender(new CEvent($this));
            }
            return $result;
        }
    }
    // }}}

    // public redirect(url,terminate=true,statusCode=302) {{{ 
    /**
     * redirect
     * Rest redirect
     * 
     * @param mixed $url 
     * @param mixed $terminate 
     * @param int $statusCode 
     * @access public
     * @return void
     */
    public function redirect($url, $terminate = true, $statusCode = 302)
    {
        if (($behavior = $this->asa('restAPI')) && $behavior->getEnabled()) {
            $this->redirectRest($url, $terminate, $statusCode);
        } else {
            if ($this->hasEventHandler('onBeforeRedirect')) {
                $this->onBeforeRedirect(new CEvent($this));
            }
            parent::redirect($url, $terminate, $statusCode);
        }
    }
    // }}}

    public function init()
    {
        Yii::app()->theme = 'mm_secure';

        if (isset(Yii::app()->events)) {
            Yii::app()->events->initController($this);
        }

        parent::init();
    }

    public function addStatisticCookie($method, $event, $params = array())
    {
        if (defined('COOKIE_STAT_ENABLED') && constant('COOKIE_STAT_ENABLED')) {
            // NOTE: @ak disabled, but if it really need - make here colllecting and set when render
            Yii::app()->request->cookies[$method] = new CHttpCookie($method, json_encode(array(
                        array($event => $params)
            )));
        }
    }


    public function actionError()
    {
        $this->layout = 'small';
        $this->render('application.views.site.error', Yii::app()->errorHandler->error);
    }

    public function onBeforeRender($event)
    {
        $this->raiseEvent('onBeforeRender', $event);
    }
    public function onAfterRender($event)
    {
        $this->raiseEvent('onAfterRender', $event);
    }
    public function onBeforeRedirect($event)
    {
        $this->raiseEvent('onBeforeRedirect', $event);
    }
}
